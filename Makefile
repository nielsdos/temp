-include config.mak

MSG = @toolchain/msg
QEMU = qemu-system-x86_64
QEMU_FLAGS = -drive format=raw,file=disk -m 128 \
				-smp 4 -numa node,cpus=3 -numa node,cpus=2 -numa node,cpus=0-1 \
				-numa dist,src=0,dst=1,val=20 -numa dist,src=1,dst=2,val=30 -numa dist,src=0,dst=2,val=15
BOCHS = bochs
BOCHS_FLAGS = -q

.PHONY: all info files kernel kernel-debug bochs qemu qemu-debug vbox clean linecount

# Check if configurated and toolchain is available
ifneq ($(TARGET),)
ifeq ($(shell test -e $(PREFIX)/bin/$(CC) && echo x),x)
all: kernel files
else
all:
	$(error You need to build a toolchain. Use the instructions in the README to build one.)
endif
else
all:
	$(error You should run ./configure to generate the configuration files and then follow the instructions in the README.)
endif

info:
	$(MSG) Building for $(ARCH) in $(MODE) mode

files:
	@# Updates files in mount if files have been changed or files in mount are missing
	@cp -ur "$(SYSROOT)"/*/ mnt/

kernel: info
	@$(MAKE) -C kernel

kernel-debug: info
	@$(MAKE) -C kernel kernel-debug

bochs: kernel
	@$(BOCHS) $(BOCHS_FLAGS)

qemu: kernel
	@$(QEMU) $(QEMU_FLAGS) -enable-kvm

qemu-debug: kernel-debug
	@$(QEMU) -s -S $(QEMU_FLAGS)

vbox: kernel
	@-VBoxManage unregistervm "$(VBOX_VM_NAME)" --delete 2>/dev/null
	@# TODO: 64bit other
	@VBoxManage createvm --name "$(VBOX_VM_NAME)" --ostype Other --register
	@VBoxManage modifyvm "$(VBOX_VM_NAME)" --memory 1024 --vram 32 --audiocontroller ac97 --ioapic on --cpus 2 --audioout on
	@VBoxManage storagectl "$(VBOX_VM_NAME)" --add ide --name IDE
	@rm -f disk.vdi
	@VBoxManage convertdd disk disk.vdi --format VDI
	@VBoxManage storageattach "$(VBOX_VM_NAME)" --storagectl IDE --medium disk.vdi --type hdd --port 0 --device 0
	@VBoxManage startvm "$(VBOX_VM_NAME)"

clean:
	@$(MAKE) -C kernel clean

linecount:
	@(git ls-files | xargs wc -l | sort -n) 2> /dev/null
