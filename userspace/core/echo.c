#include <stdio.h>
#include <err.h>
#include <string.h>

int main(int argc, char* argv[])
{
    (void)argc;
    ++argv;

    int newline = 1;

    if(*argv && !strcmp(*argv, "-n"))
    {
        ++argv;
        newline = 0;
    }

    while(*argv)
    {
        fputs(*argv++, stdout);
        if(*argv)
            putchar(' ');
    }

    if(newline)
        putchar('\n');

    if(ferror(stdout) || fflush(stdout) == EOF)
    {
        err(1, "<stdout>");
        return 1;
    }

    return 0;
}
