#include <stdio.h>

int main(int argc, char* argv[])
{
    // This program works fast enough for its purpose.
    // That's the reason this does not use a buffer to get an output
    // as fast as possible.

    if(argc == 1)
    {
        while(1)
            printf("y\n");
    }
    else
    {
        while(1)
        {
            for(int i = 1; i < argc - 1; ++i)
                printf("%s ", argv[i]);
            
            printf("%s\n", argv[argc - 1]);
        }
    }

    return 0;
}
