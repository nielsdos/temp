#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <errno.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

#define unlikely(p) __builtin_expect(!!(p), 0)

#define BUFFER_SIZE (1024 * 3)

static const char base64Table_[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

// Inverse index table: ascii symbol -> base64 index (255 means invalid)
static const uint8_t indexTable_[] =
{
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,  62, 255, 255, 255,  63,
    52,   53,  54,  55,  56,  57,  58,  59,  60,  61, 255, 255, 255,   0, 255, 255,
    255,   0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
    15,   16,  17,  18,  19,  20,  21,  22,  23,  24,  25, 255, 255, 255, 255, 255,
    255,  26,  27,  28,  29,  30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,
    41,   42,  43,  44,  45,  46,  47,  48,  49,  50,  51, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255
};

static int isbase64(int c)
{
    return isalnum(c) || c == '+' || c == '/' || c == '=';
}

static size_t min(size_t a, size_t b)
{
    return (a < b) ? a : b;
}

static void decode(FILE* in, int ignoreGarbage)
{
    uint8_t inBuffer[BUFFER_SIZE / 3 * 4];
    uint8_t outBuffer[BUFFER_SIZE];
    size_t  inOffset = 0;

    do
    {
        size_t readTotal = inOffset;
        do
            readTotal += fread(inBuffer + readTotal, 1, sizeof(inBuffer) - readTotal, in);
        while(!feof(in) && !ferror(in) && readTotal < sizeof(inBuffer));

        if(ignoreGarbage)
        {
            size_t i = 0;
            while(i < readTotal)
            {
                if(isbase64(inBuffer[i]))
                    ++i;
                else
                {
                    size_t j = i;
                    do
                        ++j;
                    while(!isbase64(inBuffer[j]) && j < readTotal);
                    memmove(inBuffer + i, inBuffer + j, readTotal - j);
                    readTotal -= j - i;
                }
            }
        }

        if(readTotal != 0)
        {
            size_t inPos = 0;
            size_t outPos = 0;
            int invalid = 0;

            #define IN_BOUNDS (readTotal - inPos >= 4)

            while(IN_BOUNDS)
            {
                // Handle "multiple of 4 byte newline"
                while(inBuffer[inPos] == '\n' && IN_BOUNDS)
                    ++inPos;

                // '=' only possible in two last positions
                if(inBuffer[inPos + 0] == '=' || inBuffer[inPos + 1] == '='
                    || (!feof(in) && (inBuffer[inPos + 2] == '=' || inBuffer[inPos + 3] == '=')))
                {
                    invalid = 1;
                    break;
                }

                uint8_t b0 = indexTable_[inBuffer[inPos + 0]];
                uint8_t b1 = indexTable_[inBuffer[inPos + 1]];
                uint8_t b2 = indexTable_[inBuffer[inPos + 2]];
                uint8_t b3 = indexTable_[inBuffer[inPos + 3]];

                if(unlikely(b0 == 0xFF || b1 == 0xFF || b2 == 0xFF || b3 == 0xFF))
                {
                    invalid = 1;
                    break;
                }

                uint32_t n = b0 << 18 | b1 << 12 | b2 << 6 | b3;

                outBuffer[outPos + 0] = n >> 16;
                outBuffer[outPos + 1] = n >> 8 & 0xFF;
                outBuffer[outPos + 2] = n & 0xFF;

                inPos  += 4;
                outPos += 3;
            }

            #undef IN_BOUNDS

            // Can happen due to handling newlines (at most 4 bytes)
            inOffset = readTotal - inPos;
            if(inOffset)
            {
                // We need to get those bytes to the front of the input buffer and offset the input
                for(size_t i = inPos; i < readTotal; ++i)
                    inBuffer[i - inPos] = inBuffer[i];
            }

            if(fwrite(outBuffer, 1, outPos, stdout) < outPos)
                err(1, "<stdout>");

            if(invalid)
            {
                fflush(stdout);
                errx(1, "invalid input");
            }
        }
    }
    while(!feof(in) && !ferror(in));
}

static void encode(FILE* in, size_t wrap)
{
    uint8_t inBuffer[BUFFER_SIZE];
    uint8_t outBuffer[BUFFER_SIZE / 3 * 4];
    size_t  column = 0;

    do
    {
        size_t readTotal = 0;
        do
            readTotal += fread(inBuffer + readTotal, 1, sizeof(inBuffer) - readTotal, in);
        while(!feof(in) && !ferror(in) && readTotal < sizeof(inBuffer));

        if(readTotal != 0)
        {
            size_t inPos = 0;
            size_t outPos = 0;

            while(readTotal - inPos >= 3)
            {
                outBuffer[outPos + 0] = base64Table_[inBuffer[inPos + 0] >> 2];
                outBuffer[outPos + 1] = base64Table_[((inBuffer[inPos + 0] & 0b11) << 4) | (inBuffer[inPos + 1] >> 4)];
                outBuffer[outPos + 2] = base64Table_[((inBuffer[inPos + 1] & 0b1111) << 2) | (inBuffer[inPos + 2] >> 6)];
                outBuffer[outPos + 3] = base64Table_[inBuffer[inPos + 2] & 0b111111];

                inPos  += 3;
                outPos += 4;
            }

            if(readTotal - inPos != 0)
            {
                outBuffer[outPos + 0] = base64Table_[inBuffer[inPos + 0] >> 2];
                
                if(readTotal - inPos == 1)
                {
                    outBuffer[outPos + 1] = base64Table_[((inBuffer[inPos + 0] & 0b11) << 4)];
                    outBuffer[outPos + 2] = '=';
                }
                else
                {
                    outBuffer[outPos + 1] = base64Table_[((inBuffer[inPos + 0] & 0b11) << 4) | (inBuffer[inPos + 1] >> 4)];
                    outBuffer[outPos + 2] = base64Table_[((inBuffer[inPos + 1] & 0b1111) << 2)];
                }

                outBuffer[outPos + 3] = '=';
                outPos += 4;
            }

            if(wrap)
            {
                size_t i = 0;
                while(i < outPos)
                {
                    size_t remaining = wrap - column;
                    size_t toWrite = min(remaining, outPos - i);

                    if(toWrite)
                    {
                        if(fwrite(outBuffer + i, 1, toWrite, stdout) < toWrite)
                            err(1, "<stdout>");

                        column += toWrite;
                        i += toWrite;
                    }
                    else
                    {
                        if(fputc('\n', stdout) == EOF)
                            err(1, "<stdout>");

                        column = 0;
                    }
                }
            }
            else
            {
                if(fwrite(outBuffer, 1, outPos, stdout) < outPos)
                    err(1, "<stdout>");
            }
        }
    }
    while(!feof(in) && !ferror(in));

    if(wrap)
        if(fputc('\n', stdout) == EOF)
            err(1, "<stdout>");
}

int main(int argc, char* argv[])
{
    static struct option long_options[] =
    {
        { "decode",         no_argument,       0, 'd' },
        { "ignore-garbage", no_argument,       0, 'i' },
        { "wrap",           required_argument, 0, 'w' },
        { 0,                0,                 0, 0   }
    };

    size_t wrap = 76;
    int ignoreGarbage = 0;
    int doDecode = 0;
    char* endp;

    int opt;
    while((opt = getopt_long(argc, argv, "diw:", long_options, NULL)) != -1)
    {
        switch(opt)
        {
            case 'w':
                if(optarg[0] == '-')
                    errx(1, "wrap size cannot be negative");
                wrap = strtoull(optarg, &endp, 10);
                if(errno)
                    err(1, "invalid wrap size: %s", optarg);
                if(*endp)
                    errx(1, "invalid wrap size: %s", optarg);

                break;

            case 'i':
                ignoreGarbage = 1;
                break;

            case 'd':
                doDecode = 1;
                break;

            default:
                fprintf(stderr, "usage: %s [-di] [-w wrapsize] [file]\n", argv[0]);
                return 1;
        }
    }

    argc -= optind;
    argv += optind;

    if(argc > 1)
        errx(1, "extra operand: %s", *argv);

    FILE* in;
    if(argc && strcmp(*argv, "-"))
    {
        in = fopen(*argv, "rb");
        if(!in)
            err(1, "%s", *argv);
    }
    else
    {
        in = stdin;
    }

    if(doDecode)
        decode(in, ignoreGarbage);
    else
        encode(in, wrap);

    if(ferror(in))
        err(1, "read error");

    fclose(in);
    return 0;
}
