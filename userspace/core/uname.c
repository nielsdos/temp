#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <err.h>
#include <errno.h>
#include <sys/utsname.h>

enum
{
    FLAG_M = 1 << 0,
    FLAG_N = 1 << 1,
    FLAG_R = 1 << 2,
    FLAG_S = 1 << 3,
    FLAG_V = 1 << 4
};

int main(int argc, char* argv[])
{
    static struct option long_options[] =
    {
        { "all",                no_argument, 0, 'a' },
        { "machine",            no_argument, 0, 'm' },
        { "nodename",           no_argument, 0, 'n' },
        { "kernel-release",     no_argument, 0, 'r' },
        { "kernel-name",        no_argument, 0, 's' },
        { "kernel-version",     no_argument, 0, 'v' },
        { "operating-system",   no_argument, 0, 'o' },
        { 0,                    0,           0, 0   }
    };

    int flags = 0;

    int opt;
    while((opt = getopt_long(argc, argv, "amnrsvo", long_options, NULL)) != -1)
    {
        switch(opt)
        {
            case 'a':
                flags = FLAG_M | FLAG_N | FLAG_R | FLAG_S | FLAG_V;
                break;
            
            case 'm':
                flags |= FLAG_M;
                break;
            
            case 'n':
                flags |= FLAG_N;
                break;
            
            case 'r':
                flags |= FLAG_R;
                break;
            
            case 'o':
            case 's':
                flags |= FLAG_S;
                break;
            
            case 'v':
                flags |= FLAG_V;
                break;

            default:
                fprintf(stderr, "usage: %s [-amnrsov]\n", argv[0]);
                return 1;
        }
    }

    argc -= optind;
    argv += optind;

    if(argc)
        errx(1, "extra operand: %s", *argv);

    if(!flags)
        flags = FLAG_S;
    
    static struct utsname utsname;
    if(uname(&utsname) < 0)
        err(1, "uname");

    int space = 0;
    #define PRINT_COMPONENT(cond, str)                      \
                            if(flags & cond)                \
                            {                               \
                                if(space) putchar(' ');     \
                                space = 1;                  \
                                printf("%s", str);          \
                            }
    
    PRINT_COMPONENT(FLAG_S, utsname.sysname);
    PRINT_COMPONENT(FLAG_N, utsname.nodename);
    PRINT_COMPONENT(FLAG_R, utsname.release);
    PRINT_COMPONENT(FLAG_V, utsname.version);
    PRINT_COMPONENT(FLAG_M, utsname.machine);

    #undef PRINT_COMPONENT

    putchar('\n');

    return 0;
}
