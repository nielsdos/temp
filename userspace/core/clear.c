#include <unistd.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    // TODO: also use 3J to clear scrollback buffer?
    const char s[] = "\e[H\e[2J";
    write(STDOUT_FILENO, s, sizeof(s));
    fflush(stdout);
    return 0;
}