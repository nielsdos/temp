/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <cstdint>
#include <stddef.h>
#include <mem.hpp>

#if defined(__i386__) || defined(__x86_64__)
#include <arch/x86-common/vmm.hpp>
#else
#error "Unknown platform"
#endif

namespace mem::vmm
{

/**
 * Enable VMM.
 */
void Enable();

/**
 * Gets the physical address from a virtual address
 * @param vaddr The virtual address
 * @return The physical address
 */
uintptr_t GetPhysicalAddress(uintptr_t vaddr);

/**
 * Maps a page if not mapped already.
 * Uses the frames provided by the frames array if extra frames are required.
 * @param vaddr The virtual address
 * @param frames The array of frames to use to do the mapping
 * @param flags The flags
 * @return The amount of frames used
 */
uint32_t MapWithDonatedFrames(uintptr_t vaddr, uintptr_t frames[2], mapflags_t flags);

/**
 * Maps a page
 * @param vaddr The virtual address
 * @param paddr The physical address
 * @param flags The flags
 * @return True if successfully mapped
 */
bool Map(uintptr_t vaddr, uintptr_t paddr, mapflags_t flags);

/**
 * Unmaps a page
 * @param vaddr The virtual address
 */
void UnMap(uintptr_t vaddr);

/**
 * Maps a page range
 * @param vaddr The virtual address
 * @param paddr The physical address
 * @param size The range size
 * @param flags The flags
 * @return True if successfully mapped
 */
bool MapRange(uintptr_t vaddr, uintptr_t paddr, size_t size, mapflags_t flags);

/**
 * Unmaps a page range
 * @param vaddr The virtual address
 * @param size The range size
 */
void UnmapRange(uintptr_t vaddr, size_t size);

}
