/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <stddef.h>
#include <cstdint>
#include <multiboot.hpp>
#include <mem.hpp>

namespace mem
{

struct MemDomains;

namespace pmm
{

struct PMMDomain;

/**
 * Allocate contiguous pages without locking
 * @param count The amount of contiguous pages
 * @return The address
 */
uintptr_t AllocContiguousUnlocked(uint16_t size);

/**
 * Allocate contiguous pages
 * @param count The amount of contiguous pages
 * @return The address
 */
uintptr_t AllocContiguous(uint16_t size);

/**
 * Free allocated contiguous pages at address addr without locking
 * @param addr The address
 */
void FreeContiguousUnlocked(uintptr_t addr);

/**
 * Free allocated contiguous pages at address addr
 * @param addr The address
 */
void FreeContiguous(uintptr_t addr);

/**
 * Get one page of physical memory without locking
 * @return The address if succeeded, 0 otherwise
 */
uintptr_t AllocPageUnlocked();

/**
 * Get one page of physical memory without locking from a domain
 * @param domain The domain
 * @return The address if succeeded, 0 otherwise
 */
uintptr_t AllocPageUnlocked(PMMDomain* domain);

/**
 * Get one page of physical memory
 * @return The address if succeeded, 0 otherwise
 */
uintptr_t AllocPage();

/**
 * Get one page of physical memory from a domain
 * @param domain The domain
 * @return The address if succeeded, 0 otherwise
 */
uintptr_t AllocPage(PMMDomain* domain);

/**
 * Free one page of physical memory
 * @param paddr The physical address
 */
void FreePage(uintptr_t paddr);

/**
 * Initialize without memory domains
 * @param mmap The memory map
 * @param kernelEnd The end address of the kernelspace
 */
void Init(multiboot::TagMMap* mmap, uintptr_t kernelEnd);

}

}
