/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <cstdint>

namespace multiboot
{

constexpr uint32_t MAGIC     = 0x36D76289;
constexpr uint32_t ALIGNMENT = 8;

enum class TagType : uint32_t
{
    END = 0,
    CMDLINE,
    LOADER_NAME,
    MODULE,
    BASIC_MEMINFO,
    BOOTDEVICE,
    MMAP,
    VBE,
    LFB,
    ELF_SECTIONS,
    APM,
    EFI32,
    EFI64,
    SMBIOS,
    ACPI_OLD,
    ACPI,
    NETWORK,
    EFI_MMAP,
    EFI_BootServices,
    EFI32_IMAGE,
    EFI64_IMAGE,
    LOAD_BASE_ADDR
};

enum class MMapType : uint32_t
{
    AVAILABLE = 1,
    RESERVED,
    ACPI_RECLAIMABLE,
    NVS,
    BADRAM
};

struct MMapEntry
{
    uint64_t Address;
    uint64_t Length;
    MMapType Type;
    uint32_t Zero;
};

struct TagHeader
{
    TagType  Type;
    uint32_t Size;
};

struct TagCommandLine
{
    TagHeader Header;
    char      String[];
};

struct TagModule
{
    TagHeader Header;
    uint32_t  ModuleStart;
    uint32_t  ModuleEnd;
    char      CommandLine[];
};

struct TagMMap
{
    TagHeader Header;
    uint32_t  EntrySize;
    uint32_t  EntryVersion;
    MMapEntry Entries[];
};

enum class LFBType : uint8_t
{
    INDEXED,
    RGB,
    EGA_TEXT
};

struct Color
{
    uint8_t Red;
    uint8_t Green;
    uint8_t Blue;
};

struct TagLFB
{
    TagHeader Header;
    uint64_t  Address;
    uint32_t  Pitch;
    uint32_t  Width;
    uint32_t  Height;
    uint8_t   Depth;
    LFBType   Type;
    uint16_t  Reserved;

    union
    {
        struct
        {
            uint16_t NumColors;
            Color    Palette[];
        } Palette;

        struct
        {
            uint8_t  RedFieldPosition;
            uint8_t  RedMaskSize;
            uint8_t  GreenFieldPosition;
            uint8_t  GreenMaskSize;
            uint8_t  BlueFieldPosition;
            uint8_t  BlueMaskSize;
        } RGB;
    };
};

struct TagACPI
{
    TagHeader Header;
    uint8_t   RSDP[];
};

}
