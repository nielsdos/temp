/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <stddef.h>
#include <stdarg.h>
#include <multiboot.hpp>

namespace log
{

class Logger
{
public:
    virtual ~Logger();

    /**
     * Write a buffer
     * @param size The size
     */
    void Write(const char* buffer, size_t size);

protected:
    /**
     * Puts a character
     * @param c The character
     */
    virtual void PutChar(char /*c*/) = 0;
};

/**
 * Initializes the screen logger
 * @param lfb The LFB tag
 */
void InitScreenLogger(const multiboot::TagLFB* lfb);

/**
 * Sets the logger
 * @param logger The logger
 */
void SetLogger(Logger* logger);

[[gnu::format(printf, 1, 0)]]
int VLogf(const char* format, va_list list);
[[gnu::format(printf, 1, 2)]]
int Logf(const char* __restrict format, ...);

#define LOG(x, ...) log::Logf("[" LOG_PREFIX "] " x, ##__VA_ARGS__)

// Log verbose
#ifdef NDEBUG
#define LOGV(x, ...) ((void)0)
#else
#define LOGV(x, ...) LOG(x, ##__VA_ARGS__)
#endif

}
