/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <cstdint>
#include <type_traits>
#include <utility>
#include <new>
#include <assert.h>
#include <compiler.hpp>

template<typename T, uint32_t N>
class StaticArrayList
{
    typename std::aligned_storage<sizeof(T), alignof(T)>::type Data_[N];
public:
    constexpr StaticArrayList()
        : Data_{}, Size_(0)
    {
    }

    StaticArrayList(const StaticArrayList&) = delete;

    ~StaticArrayList()
    {
        for(uint32_t i = 0; i < Size_; ++i)
            std::launder(reinterpret_cast<T*>(Data_ + i))->~T();
    }

    template<typename... Args>
    bool EmplaceSorted(Args&&... args)
    {
        if(unlikely(Size_ == N))
            return false;

        T x(std::forward<Args>(args)...);

        uint32_t i = 0;
        while(i < Size_ && reinterpret_cast<T*>(Data_)[i] < x)
            ++i;

        for(uint32_t j = Size_; j > i; --j)
            Data_[j] = std::move(Data_[j - 1]);

        reinterpret_cast<T*>(Data_)[i] = std::move(x);
        ++Size_;

        return true;
    }

    template<typename... Args>
    T* EmplaceBack(Args&&... args)
    {
        if(unlikely(Size_ == N))
            return nullptr;

        new(Data_ + Size_) T(std::forward<Args>(args)...);
        return reinterpret_cast<T*>(Data_ + Size_++);
    }

    uint32_t GetSize() const
    {
        return Size_;
    }

    T* begin()
    {
        return reinterpret_cast<T*>(&Data_[0]);
    }

    const T* begin() const
    {
        return reinterpret_cast<const T*>(&Data_[0]);
    }

    T* end()
    {
        return reinterpret_cast<T*>(&Data_[Size_]);
    }

    const T* end() const
    {
        return reinterpret_cast<const T*>(&Data_[Size_]);
    }

    T& operator[](uint32_t pos)
    {
        assert(pos < Size_);
        return *std::launder(reinterpret_cast<T*>(Data_ + pos));
    }

    const T& operator[](uint32_t pos) const
    {
        assert(pos < Size_);
        return *std::launder(reinterpret_cast<const T*>(Data_ + pos));
    }

private:
    uint32_t Size_;
};
