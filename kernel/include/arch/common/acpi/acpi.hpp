/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <cstdint>

namespace arch
{

namespace acpi
{

struct RSDPDescriptor;

struct SDTHeader
{
    union
    {
        uint32_t Num;
        char     Name[4];
    }        Signature;
    uint32_t Length;
    uint8_t  Revision;
    uint8_t  Checksum;
    char     OEMID[6];
    char     OEMTableID[8];
    uint32_t OEMRevision;
    uint32_t CreatorID;
    uint32_t CreatorRevision;
} __attribute__((packed));

template<typename H, typename S>
class ACPITable
{
public:
    constexpr ACPITable()
        : Header_(nullptr)
    {
    }

    constexpr ACPITable(const H* header)
        : Header_(header)
    {
    }

    H* operator->() const
    {
        return Header_;
    }

    class Iterator
    {
    public:
        constexpr Iterator(uintptr_t pos)
            : Pos_(pos)
        {
        }

        S* operator->() const
        {
            return (S*)Pos_;
        }

        S& operator*() const
        {
            return *(S*)Pos_;
        }

        Iterator& operator++()
        {
            Pos_ = Pos_ + ((S*)Pos_)->Length;
            return *this;
        }

        bool operator!=(const Iterator& o) const
        {
            return Pos_ != o.Pos_;
        }

    private:
        uintptr_t Pos_;
    };

    Iterator begin() const
    {
        return Iterator((uintptr_t)Header_ + sizeof(H));
    }

    Iterator end() const
    {
        return Iterator((uintptr_t)Header_ + Header_->Header.Length);
    }

private:
    const H* Header_;
};

/**
 * Returns true if the SDT is valid
 * @param header Pointer to the header
 * @return True if valid, false otherwise
 */
bool IsValidSDT(const SDTHeader* header);

/**
 * Maps an SDT
 * @param header The SDT header
 * @return The mapped SDT
 */
const SDTHeader* MapSDT(const SDTHeader* header);

/**
 * Unmaps an SDT
 * @param header The SDT Header
 * @return True if successful, false otherwise
 */
bool UnmapSDT(const SDTHeader* header);

/**
 * Initializes ACPI
 * @param rsdp The RSDP
 */
void Init(const RSDPDescriptor* rsdp);

}

}
