/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <cstdint>

#if defined(__i386__) || defined(__x86_64__)
#include <arch/x86-common/arch.hpp>
#else
#error "Unknown architecture"
#endif

namespace arch
{

typedef uint32_t cpuid_t;

/**
 * Early init arch components
 * @param rsdp ACPI RSDP virtual memory address
 */
void EarlyInit(uintptr_t rsdp);

/**
 * Late init arch components (after PMM, VMM and heap are set up)
 */
void LateInit();

/**
 * Hints the CPU we're spinning
 */
void SpinHint();

/**
 * Disable interrupts
 */
void NoInt();

/**
 * Halt
 */
void Halt();

/**
 * Gets the amount of alive CPUs
 * @return The amount of alive CPUs
 */
cpuid_t GetAliveCPUCount();

/**
 * Broadcasts an IPI
 * @param vector The interrupt vector
 */
void BroadcastIPI(intvector_t vector);

/**
 * Sets the BSP Id
 * @param id The Id
 */
void SetBSPId(cpuid_t id);

/**
 * Gets the BSP Id
 * @return The Id
 */
cpuid_t GetBSPId();

/**
 * Sets the BSP domain
 * @param domain The BSP domain
 */
void SetBSPDomain(uint32_t domain);

/**
 * Gets the BSP domain
 * @return The BSP domain
 */
uint32_t GetBSPDomain();

}
