/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <cstdint>
#include <tuple.hpp>

/**
 * This header should provide CPU_SET & CPU_GET.
 */
#if defined(__i386__) || defined(__x86_64__)
#include <arch/x86-common/cpu.hpp>
#else
#error "Unknown platform"
#endif

namespace mem::pmm
{

struct PMMDomain;

}

namespace arch::cpu
{

typedef uint32_t cpuid_t;

// Per-CPU data
struct CPU
{
    cpuid_t Id;
    bool Alive;
    uint32_t Domain; // TODO?
    mem::pmm::PMMDomain* MemDomain;
} __attribute__((aligned(FALSE_SHARING_THRESHOLD)));

// TODO
void Lel();

/**
 * Initializes the BSP
 * @param cpu The CPU data structure
 */
void InitBSP(cpu::CPU* cpu);

/**
 * Sets the CPUs
 * @param cpus The CPUs
 * @param cpuCount The amount of CPUs
 */
void SetCPUs(cpu::CPU* cpus, cpuid_t cpuCount);

/**
 * Gets the CPUs array start and end
 * @return The CPUs (first & last)
 */
Tuple<cpu::CPU*, cpu::CPU*> GetCPUs();

/**
 * Init from AP
 */
void InitAP();

/**
 * Gets the current CPU Id
 * @return The current CPU Id
 */
cpuid_t GetCurrentId();

/**
 * Returns true if the CPU data is initialized
 * @return True if CPU data is initialized
 */
bool IsCPUDataInitialized();

}
