/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <bits/wordsize.h>

namespace mem
{

constexpr uintptr_t AP_TRAMPOLINE = 0x1000;

constexpr uint32_t  PAGE_MASK     = 0xFFF;
constexpr size_t    PAGE_SIZE     = 0x1000;
constexpr size_t    PAGE_SHIFT    = 12;

/**
 * With 16-bit numbers we can represent a contiguous area of 64GB.
 * That's more than enough.
 */
constexpr size_t PMM_CONT_SIZE       = 4 * 1024 * 1024;
constexpr size_t PMM_CONT_ROOT_SIZE  = PMM_CONT_SIZE / PAGE_SIZE;
constexpr size_t PMM_CONT_NODE_COUNT = PMM_CONT_ROOT_SIZE * 2 - 1;
constexpr size_t PMM_CONT_TREE_SIZE  = sizeof(uint16_t) * PMM_CONT_NODE_COUNT;
constexpr size_t PMM_CONT_TREE_PAGES = (PMM_CONT_TREE_SIZE + PAGE_MASK) / PAGE_SIZE;

#if __WORDSIZE == 32

/**
 * Physical memory on 32-bit can be at most 1024 * 1024 * 1024 * 4 bytes.
 * That is 1024 * 1024 pages (upper bound).
 * We can hold 32 pages in one entry => we need 1024 * 1024 / 32 entries.
 * One entry is 8 bytes => 1024 * 1024 / 32 * 8 bytes = 256KB
 */
constexpr size_t PMM_ENTRIES_MAX_SIZE = 256 * 1024;

constexpr uintptr_t PAGE_TBL_BASE_ADDR = 0xFFC00000;

constexpr uintptr_t LAPIC_ADDRESS    = 0xC0000000;
constexpr uintptr_t HEAP_START       = 0xC0001000;
uint32_t* const     PAGE_DIR         = (uint32_t*)0xFFFFF000;
uint32_t* const     PAGE_TBL_BASE    = (uint32_t*)PAGE_TBL_BASE_ADDR;
constexpr uintptr_t PMM_ENTRIES      = PAGE_TBL_BASE_ADDR - PMM_ENTRIES_MAX_SIZE;
constexpr uintptr_t PMM_CONT_TREE    = PMM_ENTRIES - (PMM_CONT_TREE_PAGES * PAGE_SIZE);
constexpr uintptr_t HEAP_MAX_END     = PMM_CONT_TREE;

constexpr uintptr_t AP_CPU_COUNT     = 0x7C00;
constexpr uintptr_t AP_PAGEDIR       = 0x7C04;
constexpr uintptr_t AP_START_POINTER = 0x7C08;
constexpr uintptr_t AP_CPU_DATA      = 0x7C0C;
constexpr uintptr_t AP_FIRST_DATA    = 0x7C10;

#elif __WORDSIZE == 64

#error "Not yet implemented"

#endif

}