/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

namespace arch
{

typedef uint8_t intvector_t;

/**
 * High 4 bits are interrupt-priority class.
 * The higher, the higher the priority.
 * Then the lower 4 bits determine the priority within one class.
 */
constexpr intvector_t VECTOR_IPI_HALT = 0xFE;
constexpr intvector_t VECTOR_SPURIOUS = 0xFF;

namespace x86
{

enum class ECXFeature
{
    X2APIC = 1U << 21
};

enum class ExtendedECXFeature
{
    UMIP = 1U << 2
};

enum class EDXFeature
{
    APIC = 1U << 9
};

enum class MSR : uint32_t
{
    APIC_BASE = 0x0000001B,
    FS_BASE   = 0xC0000100,
    GS_BASE   = 0xC0000101
};

/**
 * Executes the x86 cpuid instruction
 * @param level The level
 * @param eax[out] eax result
 * @param ebx[out] ebx result
 * @param ecx[out] ecx result
 * @param edx[out] edx result
 */
void CPUID(uint32_t level, uint32_t* eax, uint32_t* ebx, uint32_t* ecx, uint32_t* edx);

/**
 * Returns true if the boot CPU supports the feature f
 * @param f The feature to check support for
 * @return True if supported, false if not
 */
bool BootCPUSupports(ECXFeature f);
bool BootCPUSupports(ExtendedECXFeature f);
bool BootCPUSupports(EDXFeature f);

/**
 * Reads an MSR register
 * @param reg The MSR register
 * @param eax[out] eax result
 * @param edx[out] edx result
 */
void ReadMSR(uint32_t reg, uint32_t* eax, uint32_t* edx);
void ReadMSR(MSR reg, uint32_t* eax, uint32_t* edx);

/**
 * Writes an MSR register
 * @param reg The MSR register
 * @param eax The eax input register
 * @param edx The edx input register
 */
void WriteMSR(uint32_t reg, uint32_t eax, uint32_t edx);
void WriteMSR(MSR reg, uint32_t eax, uint32_t edx);

/**
 * Reads an MSR register
 * @param reg The MSR register
 * @return The contents
 */
uint64_t ReadMSR(MSR reg);

}

}
