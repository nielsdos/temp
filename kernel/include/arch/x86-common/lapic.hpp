/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <arch/common/arch.hpp>

namespace arch::cpu
{

struct CPU;

}

namespace arch::x86::lapic
{

/**
 * The timing does not have to be exact.
 * This means we can cheat: we can start multiple CPUs at once.
 */
constexpr unsigned int CPU_MAX_START_COUNT = 16;

/**
 * Prepare the LAPIC
 * @param phys The physical address
 */
void Prepare(uintptr_t phys);

/**
 * Sets up and initializes the LAPIC system in the BSP
 */
void BSPSetup();

/**
 * Inits the LAPIC for this CPU
 */
void Init();

/**
 * Sends an IPI to a CPU
 * @param cpuid The Id of the destination CPU
 * @param vector The vector
 */
void SendIPI(cpuid_t cpuid, intvector_t vector);

/**
 * Broadcasts an IPI to all CPUs exclusive the sender
 * @param vector The vector
 */
void BroadcastIPI(intvector_t vector);

/**
 * Starts up the provided CPUs
 * @param first First CPU
 * @param last Last CPU
 */
void StartCPUs(cpu::CPU* first, cpu::CPU* last);

/**
 * Lets the APIC timer wait for us microseconds
 * @param us The amount of microseconds to wait
 */
void TimerWait(uint64_t us);

/**
 * Gets the current LAPIC Id
 * @return The current Id
 */
cpuid_t GetId();

}
