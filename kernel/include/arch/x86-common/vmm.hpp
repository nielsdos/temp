/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <bits/wordsize.h>

namespace mem::vmm
{

#if __WORDSIZE == 32
typedef uint32_t mapflags_t;
#elif __WORDSIZE == 64
typedef uint64_t mapflags_t;
#else
#error "What is this"
#endif

enum MapFlags : mapflags_t
{
    // Protection flags
    WRITABLE = 1 << 1,
    NO_EXEC = 0, // TODO
    GLOBAL = 1 << 8,

    // Cache control flags
    PWT = 1 << 3,
    PCD = 1 << 4,
    PAT = 1 << 7,

    // Cache flags (only one at a time is allowed)
    CACHE_WRITEBACK = 0,            // WB
    CACHE_WRITETHROUGH = PWT,       // WT
    CACHE_UNCACHED = PCD,           // UC-
    CACHE_UNCACHEABLE = PCD | PWT,  // UC
    CACHE_WRITECOMBINE = PAT,       // WC
    CACHE_WRITEPROTECT = PAT | PWT  // WP
};

/**
 * Inits VMM
 */
void Init();

/**
 * Invalidates the page at the given virtual address
 * Note: only on current CPU
 * @param The virtual address
 */
void InvalidatePage(uintptr_t addr);

/**
 * Aligns an address down to a page
 * @param addr The address
 * @return The aligned address
 */
template<typename T>
inline static T AlignDown(T addr)
{
    return (addr & ~PAGE_MASK);
}

/**
 * Aligns an address up to a page
 * @param addr The address
 * @return The aligned address
 */
template<typename T>
inline static T AlignUp(T addr)
{
    return AlignDown(addr + PAGE_MASK);
}

}
