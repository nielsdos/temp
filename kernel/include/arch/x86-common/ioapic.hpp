/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <cstdint>

namespace arch::x86::ioapic
{

class IOAPIC
{
public:
    /**
     * Creates a new I/O Apic
     * @param GSIBase The GSI base
     * @param addr The physical MMIO address
     */
    IOAPIC(uint32_t GSIBase, uintptr_t addr);

    /**
     * Gets the GSI base of this I/O APIC
     * @return The GSI base
     */
    inline uint32_t GetGSIBase() const
    {
        return GSIBase_;
    }

    /**
     * Returns true if this I/O Apic is responsible for the given GSI.
     * @param gsi The GSI
     * @return True if responsible, false otherwise
     */
    bool IsResponsibleFor(uint32_t gsi) const;

    IOAPIC* Next = nullptr;

private:
    // The registers are just 8-bit, but the I/O Apic requires 32-bit access.
    enum class Reg : uint32_t;

    /**
     * Reads data from register reg
     * @param reg The register to read from
     * @return The data in that register
     */
    uint32_t Read(Reg reg) const;

    /**
     * Writes a value to register reg
     * @param reg The register to write to
     * @param value The value to write
     */
    void Write(Reg reg, uint32_t value) const;

    uint32_t  GSIBase_;
    uint8_t   redirEntries_;
    uintptr_t virt_;
};

/**
 * Registers an I/O Apic
 * @param ioa The I/O Apic
 */
void Register(IOAPIC* ioa);

/**
 * Gets the I/O Apic responsible for the given gsi
 * @param gsi The gsi
 * @return The I/O Apic if found, nullptr otherwise
 */
IOAPIC* GetFor(uint32_t gsi);

}
