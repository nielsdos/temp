/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

namespace arch::cpu
{

/**
 * Beware of false sharing within a cache line or sector.
 * Data should be allocated with alignment of at least the "false sharing threshold".
 */
constexpr uint8_t FALSE_SHARING_THRESHOLD = 128;

#if defined(__i386__)
#define PER_CPU_SEG "fs"
#else /* __x86_64__ */
#define PER_CPU_SEG "gs"
#endif

#define CPU_ENTRY(name) ((arch::cpu::CPU*)nullptr)->name

#define CPU_GET(name) ({ \
                        __typeof__(CPU_ENTRY(name)) res; \
                        asm volatile("mov %%" PER_CPU_SEG ":%1, %0" : "=r" (res) : "m" (CPU_ENTRY(name))); \
                        res; \
                      })

#define CPU_SET_HELPER(suffix, name, value) \
            asm volatile("mov" suffix " %1, %%" PER_CPU_SEG ":%0" : "=m" (CPU_ENTRY(name)) : "r" (value))

#define CPU_SET(name, value) { \
                                 switch(sizeof(__typeof__(CPU_ENTRY(name)))) \
                                 { \
                                     case 1: CPU_SET_HELPER("b", name, value); break; \
                                     case 2: CPU_SET_HELPER("w", name, value); break; \
                                     case 4: CPU_SET_HELPER("l", name, value); break; \
                                     case 8: CPU_SET_HELPER("q", name, value); break; \
                                     default: assert(false); break; \
                                 } \
                             }


}
