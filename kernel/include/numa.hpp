/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <cstdint>
#include <staticarraylist.hpp>

namespace mem
{

/**
 * Proximity domains.
 * - They are not neccessarily contiguous.
 * - There are usually not alot of them.
 * - They don't neccessarily contain CPU *and* memory.
 * 
 * So we use a small array here for the domains.
 * 
 * Memory.
 * If memory in a proximity domain is fully used, we
 * need to get more memory from the closest domain.
 * That means we need to keep an ordered list of closest domains
 * per proximity domain.
 */
constexpr unsigned int MAX_PROX = 16;
constexpr unsigned int MAX_MEM  = 6;

struct Range
{
    uint64_t Address;
    uint64_t Length;

    constexpr Range(uint64_t address, uint64_t length)
        : Address(address), Length(length)
    {
    }

    bool operator<(const Range& o) const
    {
        return Address < o.Address;
    }
};

struct Domain
{
    uint32_t Id;
    StaticArrayList<Range, MAX_MEM> Mem;

    constexpr Domain()
        : Id(0), Mem{}
    {
    }

    constexpr Domain(uint32_t id)
        : Id(id), Mem{}
    {
    }
};

struct MemDomains
{
    StaticArrayList<Domain, MAX_PROX> Domains;

    constexpr MemDomains()
        : Domains{}
    {
    }

    /**
     * Gets a domain with given Id or creates it if it doesn't exists
     * @param id The Id
     * @return The domain or nullptr if maximum amount of domains reached
     */
    Domain* GetDomain(uint32_t id);

    /**
     * Dumps information about domains
     */
    void PrintDomains() const;
};

/**
 * Enable domains
 */
void EnableDomains();

/**
 * Gets the memory domains structure
 * @return The memory domains
 */
MemDomains* GetMemDomains();

}
