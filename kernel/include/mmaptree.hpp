/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <stddef.h>
#include <cstdint>

namespace mem
{

constexpr uintptr_t COLOR_MASK = 0b000000000001;
constexpr uintptr_t FLAGS_MASK = 0b111111111110;
constexpr uintptr_t LOWER_MASK = 0b111111111111;

/**
 * This is an interval tree, it's an augmented red-black tree as internal storage.
 * This datastructure is made to look up gaps of a given size between intervals in the tree,
 * but also to find overlapping intervals and search intervals, all in O(log n) time.
 * This tree is ordered by the start of the interval.
 * The stored intervals are in [start, end[ format.
 */
class MMapTree
{
private:
    struct Node
    {
        enum class Color : uint8_t
        {
            BLACK,
            RED
        };

    private:
        uintptr_t Start;
        // End and flags in one, we have the 12 lower bits free, color is the lowest bit
        uintptr_t EndAndFlags;

    public:
        // Maximum gap in this subtree
        size_t MaxGap;

        // Next and previous node according to ordering
        Node* Successor;
        Node* Predecessor;

        Node* Left;
        Node* Right;
        Node* Parent;

        constexpr Node(uintptr_t addr);
        Node(uintptr_t start, uintptr_t end);
        Color GetColor() const;
        void SetColor(Color c);
        uint16_t GetFlags() const;
        void SetFlags(uint16_t flags);
        uintptr_t GetStart() const;
        void SetStart(uintptr_t start);
        uintptr_t GetEnd() const;
        void SetEnd(uintptr_t end);

        /**
         * Gets gap between this interval and the successor
         * @return The gap size
         */
        size_t GetGap() const;

        /**
         * Calculates the max gap
         * @return The max gap
         */
        size_t CalculateMaxGap() const;

        /**
         * Updates the max gap
         */
        void UpdateMaxGap();
    };

public:
    MMapTree();
    MMapTree(const MMapTree&) = delete;
    ~MMapTree();

    /**
     * Adds a new interval into the tree
     * @param start The start
     * @param end The end
     * @return True if successfully added
     */
    bool Add(uintptr_t start, uintptr_t end);

    /**
     * Removes an interval from the tree
     * @param start The start
     * @param end The end
     * @return True if successfully removed, false if value not in tree
     */
    bool Remove(uintptr_t start, uintptr_t end);

    /**
     * Finds a gap of the given size
     * @param gap The size
     * @return The starting address of the gap
     */
    uintptr_t FindGap(size_t gap) const;

    /**
     * Print out the tree (debug purposes only) (sideview)
     */
    void DebugPrint() const;

private:
    /**
     * Adds a new interval into the tree
     * @param start The start
     * @param end The end
     * @return The node
     */
    Node* AddNode(uintptr_t start, uintptr_t end);

    /**
     * Update the gap in node if needed
     * @param n The node
     * @return True if this is the last node to update, false otherwise
     */
    bool UpdateGap(Node* n);

    /**
     * Update parents to update maximum gap
     * @param n The node
     */
    void FixGapInParent(Node* n);

    /**
     * Gets the left-most (minimum) node in the subtree of the given node
     * @param x The node (subtree)
     * @return The left-most node
     */
    Node* MinimumNode(Node* x) const;

    /**
     * Removes a node
     * @param n The node to remove
     * @return The successor to this node
     */
    Node* RemoveNode(Node* n);

    /**
     * Gets the node of the interval where addr lies in
     * @param addr The address
     * @return The node containing addr
     */
    Node* GetNode(uintptr_t addr) const;

    /**
     * Left rotate around a given node
     * @param x The node to rotate around
     */
    void RotateLeft(Node* x);

    /**
     * Right rotate around a given node
     * @param y The node to rotate around
     */
    void RotateRight(Node* y);

    /**
     * Fixup adding
     * @param n The node
     */
    void FixAdd(Node* n);

    /**
     * Fixup removing
     * @param n The node
     */
    void FixRemove(Node* n);

    /**
     * Print out the tree (debug purposes only)
     * @param node The node to print out
     * @param level The level of depth
     */
    void DebugPrint(const Node* node, int level) const;

    static Node nil_;
    Node* root_ = &nil_;
};

}
