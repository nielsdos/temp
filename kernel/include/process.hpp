/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <stddef.h>
#include <cstdint>
#include <mmaptree.hpp>
#include <mutex.hpp>
#include <vmm.hpp>

namespace tasking
{

class Process
{
public:
    Process(char* imagePath);
    Process(const Process&) = delete;
    ~Process();
    
    /**
     * Adds a phys-virt memory mapping
     * Note: assumes no overflow of addr + size
     * @param addr The physical address to map
     * @param size The size of the memory to map
     * @param flags The flags for mapping
     * @return The virtual address
     */
    uintptr_t AddMapping(uintptr_t addr, size_t size, mem::vmm::mapflags_t flags);

    /**
     * Allocates physical memory (non-contiguous) and maps it
     * @param size The size
     * @param flags The flags for mapping
     * @return The virtual address
     */
    uintptr_t AllocPhysicalAndMap(size_t size, mem::vmm::mapflags_t flags);

    /**
     * Removes mapping
     * @param addr The virtual address of the mapping (can be a part of a map too)
     * @param size The size of the memory to unmap
     */
    bool RemoveMapping(uintptr_t addr, size_t size);

protected:
    char*         imagePath_;
    mem::MMapTree mmapTree_;
    Process*      parent_ = nullptr;
    sync::Mutex   mapLock_;
    uint64_t      pid_;
};

/**
 * Initializes the kernel process
 * @param kernelEnd The kernel end address
 */
void InitKernelProcess(uintptr_t kernelEnd);

/**
 * Gets the kernel process
 * @return The kernel process
 */
Process* GetKernelProcess();

}
