/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdint>
#include <stddef.h>
#include <assert.h>
#include <arch/common/arch.hpp>
#include <arch/common/cpu.hpp>
#include <logger.hpp>
#include <pmm.hpp>
#include <heap.hpp>
#include <process.hpp>
#include <multiboot.hpp>
#include <version.hpp>

// TODO
#include <vmm.hpp>

extern "C"
uint8_t _ldKernelEnd[];

/**
 * Entry point of the kernel
 * @param tags The multiboot tags
 */
extern "C"
void KernelMain(multiboot::TagHeader* tag)
{
    multiboot::TagMMap* mmap = nullptr;
    multiboot::TagLFB* lfb = nullptr;
    const char* cmdLine = nullptr;
    uintptr_t rsdp = 0;
    uintptr_t kernelEnd = (uintptr_t)_ldKernelEnd;

    // Process multiboot tags
    for(; tag->Type != multiboot::TagType::END;
        tag = (multiboot::TagHeader*)((uintptr_t)tag + ((tag->Size + 7) & ~7)))
    {
        switch(tag->Type)
        {
            case multiboot::TagType::CMDLINE:
            {
                cmdLine = reinterpret_cast<multiboot::TagCommandLine*>(tag)->String;
                break;
            }

            case multiboot::TagType::ACPI_OLD:
            case multiboot::TagType::ACPI:
            {
                rsdp = (uintptr_t)reinterpret_cast<multiboot::TagACPI*>(tag)->RSDP;
                break;
            }

            case multiboot::TagType::LFB:
            {
                lfb = reinterpret_cast<multiboot::TagLFB*>(tag);
                break;
            }

            case multiboot::TagType::MMAP:
            {
                mmap = reinterpret_cast<multiboot::TagMMap*>(tag);
                break;
            }

            case multiboot::TagType::MODULE:
            {
                auto module = reinterpret_cast<multiboot::TagModule*>(tag);
                if(kernelEnd < module->ModuleEnd)
                    kernelEnd = module->ModuleEnd;

                break;
            }

            default:
                break;
        }

        uintptr_t tagEnd = (uintptr_t)tag + tag->Size;
        if(kernelEnd < tagEnd)
            kernelEnd = tagEnd;
    }

    kernelEnd = mem::vmm::AlignUp(kernelEnd);

    // TODO: this does not work yet because we need VMM enabled for this
    /*if(kernelEnd < 0x400000)
        mem::vmm::UnmapRange(kernelEnd, 0x400000 - kernelEnd);*/

    arch::EarlyInit(rsdp);
    arch::cpu::Lel();//TODO: "initBSPDomainless" ?
    mem::pmm::Init(mmap, kernelEnd);
    mem::heap::Init(); // op cpu struct delen van heap zetten, daarna percpu struct overkopieren naar heap alloced ding
    uint8_t* a = new uint8_t[64];
    uint8_t* b = new uint8_t[32+4];
    log::Logf("%p %p|%lx %lx\n", a, b, mem::vmm::GetPhysicalAddress((uintptr_t)a), mem::vmm::GetPhysicalAddress((uintptr_t)b));
    for(;;);
    tasking::InitKernelProcess(kernelEnd); // Moet dit met "new" ?
    log::InitScreenLogger(lfb);

    // Late init
    log::Logf("%s %s %s %s\n", version::SYSNAME, version::RELEASE, version::VERSION, version::MACHINE);
    log::Logf("Command line: %s\nScreen: %ux%ux%u %p\n\n", cmdLine, lfb->Width, lfb->Height, lfb->Depth, (void*)lfb->Address);
    // TODO: dump wat data hier?
    for(;;);
    arch::LateInit();
}
