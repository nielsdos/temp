/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <mmaptree.hpp>
#include <logger.hpp>
#include <vmm.hpp>
#include <compiler.hpp>

namespace mem
{

// TODO: 64bit
MMapTree::Node MMapTree::nil_ = MMapTree::Node(0xC0000000);

constexpr MMapTree::Node::Node(uintptr_t addr)
    : Start(addr), EndAndFlags(addr), MaxGap(0), Successor(&nil_), Predecessor(&nil_), Left(&nil_), Right(&nil_), Parent(&nil_)
{
    // Black node by default
    assert((addr & LOWER_MASK) == 0);
}

MMapTree::Node::Node(uintptr_t start, uintptr_t end)
    : Start(start), EndAndFlags(end), Left(&nil_), Right(&nil_)
{
    assert((start & LOWER_MASK) == 0);
    assert((end   & LOWER_MASK) == 0);
}

MMapTree::Node::Color MMapTree::Node::GetColor() const
{
    return static_cast<MMapTree::Node::Color>(EndAndFlags & COLOR_MASK);
}

void MMapTree::Node::SetColor(MMapTree::Node::Color c)
{
    EndAndFlags = (EndAndFlags & ~COLOR_MASK) | static_cast<uint8_t>(c);
}

uint16_t MMapTree::Node::GetFlags() const
{
    return (uint16_t)(EndAndFlags & FLAGS_MASK);
}

void MMapTree::Node::SetFlags(uint16_t flags)
{
    EndAndFlags = (EndAndFlags & ~LOWER_MASK) | flags;
}

uintptr_t MMapTree::Node::GetStart() const
{
    return Start;
}

void MMapTree::Node::SetStart(uintptr_t start)
{
    Start = start;
}

uintptr_t MMapTree::Node::GetEnd() const
{
    return EndAndFlags & ~LOWER_MASK;
}

void MMapTree::Node::SetEnd(uintptr_t end)
{
    assert((end & LOWER_MASK) == 0);
    EndAndFlags = (EndAndFlags & LOWER_MASK) | end;
}

size_t MMapTree::Node::GetGap() const
{
    return Successor->GetStart() - GetEnd();
}

size_t MMapTree::Node::CalculateMaxGap() const
{
    size_t max = GetGap();

    if(Left->MaxGap > max)
        max = Left->MaxGap;

    if(Right->MaxGap > max)
        max = Right->MaxGap;

    return max;
}

void MMapTree::Node::UpdateMaxGap()
{
    MaxGap = CalculateMaxGap();
}

MMapTree::MMapTree()
{
}

MMapTree::~MMapTree()
{
    Node* n = MinimumNode(root_);
    while(n != &nil_)
    {
        Node* x = n;
        n = n->Successor;
        delete x;
    }
}

bool MMapTree::Add(uintptr_t start, uintptr_t end)
{
    return AddNode(start, end);
}

MMapTree::Node* MMapTree::AddNode(uintptr_t start, uintptr_t end)
{
    assert(start < end);

    Node* y = &nil_;
    Node* x = root_;

    Node* prev = &nil_;
    Node* next = &nil_;
    while(x != &nil_)
    {
        y = x;

        // If we go left, the next in-order node was in the right subtree
        if(start < x->GetStart())
        {
            next = x;
            x = x->Left;
        }
        // If we go right, the previous in-order node was in the left subtree
        else
        {
            prev = x;
            x = x->Right;
        }
    }

    // No overlapping
    if(prev != &nil_) assert(start >= prev->GetEnd());
    if(next != &nil_) assert(end   <= next->GetStart());

    // Merging intervals
    // TODO: check if flags are the same
    bool mergeNext = (next != &nil_ && next->GetStart() == end);
    bool mergePrev = (prev != &nil_ && prev->GetEnd()   == start);

    if(mergeNext)
    {
        end = next->GetEnd();
        RemoveNode(next);
    }

    if(mergePrev)
    {
        prev->SetEnd(end);
        FixGapInParent(prev);
        return prev;
    }

    // No merging, create a new node for this interval
    Node* n = new Node(start, end);
    if(!n)
        return nullptr;

    if(y == &nil_)
        root_ = n;
    else if(n->GetStart() < y->GetStart())
        y->Left = n;
    else
        y->Right = n;
    
    n->Parent = y;
    n->SetColor(Node::Color::RED);

    n->Successor = next;
    n->Predecessor = prev;

    if(prev != &nil_) prev->Successor = n;
    if(next != &nil_) next->Predecessor = n;

    FixGapInParent(n);
    FixAdd(n);

    return n;
}

bool MMapTree::Remove(uintptr_t start, uintptr_t end)
{
    assert(start < end);

    Node* n = GetNode(start);
    if(!n)
        return false;

    do
    {
        if(start <= n->GetStart() && end >= n->GetEnd())
            n = RemoveNode(n);
        else if(start <= n->GetStart())
        {
            uintptr_t prevEnd = n->GetEnd();
            n = RemoveNode(n);

            if(end < prevEnd)
                n = AddNode(end, prevEnd)->Successor;
            else
                n = n->Successor;
        }
        else
        {
            uintptr_t prevEnd = n->GetEnd();
            n->SetEnd(start);
            FixGapInParent(n);

            if(end < prevEnd)
                n = AddNode(end, prevEnd)->Successor;
            else
                n = n->Successor;
        }
    }
    while(n != &nil_ && n->GetStart() < end);

    return true;
}

uintptr_t MMapTree::FindGap(size_t gap) const
{
    assert(gap != 0);

    Node* n = root_;
    while(n != &nil_)
    {
        if(n->GetGap() >= gap)
            return n->GetEnd();
        else if(n->Left->MaxGap >= gap)
            n = n->Left;
        else if(n->Right->MaxGap >= gap)
            n = n->Right;
        else
            return 0;
    }

    // This only happens if there are no nodes yet
    return 0;
}

void MMapTree::DebugPrint() const
{
    DebugPrint(root_, 0);
}

bool MMapTree::UpdateGap(Node* n)
{
    size_t maxGap = n->CalculateMaxGap();
    if(n->MaxGap == maxGap)
        return false;

    n->MaxGap = maxGap;

    return true;
}

void MMapTree::FixGapInParent(Node* n)
{
    while(n != &nil_ && UpdateGap(n))
        n = n->Parent;
}

MMapTree::Node* MMapTree::MinimumNode(Node* x) const
{
    while(x->Left != &nil_)
        x = x->Left;
    
    return x;
}

MMapTree::Node* MMapTree::RemoveNode(Node* n)
{
    Node* y = (n->Left == &nil_ || n->Right == &nil_) ? n : n->Successor;
    Node* x = (y->Left == &nil_) ? y->Right : y->Left;

    x->Parent = y->Parent;

    if(y->Parent == &nil_)
        root_ = x;
    else if(y == y->Parent->Left)
        y->Parent->Left = x;
    else
        y->Parent->Right = x;
    
    Node* prev = n->Predecessor;
    Node* next = n->Successor;

    Node* successor;

    if(n != y)
    {
        n->SetStart(y->GetStart());
        n->SetEnd(y->GetEnd());
        n->Successor = y->Successor;

        prev->Successor = n;
        n->Successor->Predecessor = n;

        successor = n;
    }
    else
    {
        prev->Successor = n->Successor;
        next->Predecessor = n->Predecessor;

        successor = n->Successor;
    }

    if(prev != &nil_)
        FixGapInParent(prev);

    if(y->GetColor() == Node::Color::BLACK)
        FixRemove(x);

    delete y;

    return successor;
}

MMapTree::Node* MMapTree::GetNode(uintptr_t addr) const
{
    Node* n = root_;
    while(n != &nil_)
    {
        if(addr >= n->GetStart() && addr <= n->GetEnd())
            return n;
        else if(addr < n->GetStart())
            n = n->Left;
        else
            n = n->Right;
    }

    return nullptr;
}

void MMapTree::RotateLeft(Node* x)
{
    /**
     * x                        y
     *    \                  /     \
     *       y      =>    x           A
     *     /   \            \
     *    L      A            L
     * (x->Left)
     */
    
    Node* y = x->Right;
    x->Right = y->Left;
    y->Parent = x->Parent;

    if(y->Left != &nil_)
        y->Left->Parent = x;

    if(x->Parent == &nil_)
        root_ = y;
    else if(x == x->Parent->Left)
        x->Parent->Left = y;
    else
        x->Parent->Right = y;

    x->Parent = y;
    y->Left = x;

    x->UpdateMaxGap();
    y->UpdateMaxGap();
}

void MMapTree::RotateRight(Node* y)
{
    /**
     *       y               x
     *    /     \               \
     * x          A    =>          y
     *   \                       /   \
     *     R                    R      A
     * (x->Right)
     */

    Node* x = y->Left;
    y->Left = x->Right;
    x->Parent = y->Parent;

    if(x->Right != &nil_)
        x->Right->Parent = y;

    if(y->Parent == &nil_)
        root_ = x;
    else if(y == y->Parent->Right)
        y->Parent->Right = x;
    else
        y->Parent->Left = x;

    y->Parent = x;
    x->Right = y;

    y->UpdateMaxGap();
    x->UpdateMaxGap();
}

void MMapTree::FixAdd(Node* n)
{
    while(n->Parent->GetColor() == Node::Color::RED)
    {
        Node* grandParent = n->Parent->Parent;

        if(n->Parent == grandParent->Left)
        {
            Node* uncle = grandParent->Right;
            if(uncle->GetColor() == Node::Color::RED)
            {
                n->Parent->SetColor(Node::Color::BLACK);
                uncle->SetColor(Node::Color::BLACK);
                grandParent->SetColor(Node::Color::RED);
                n = grandParent;
            }
            else
            {
                if(n == n->Parent->Right)
                {
                    n = n->Parent;
                    RotateLeft(n);
                }

                n->Parent->SetColor(Node::Color::BLACK);
                grandParent->SetColor(Node::Color::RED);
                RotateRight(grandParent);
            }
        }
        else
        {
            Node* uncle = grandParent->Left;
            if(uncle->GetColor() == Node::Color::RED)
            {
                n->Parent->SetColor(Node::Color::BLACK);
                uncle->SetColor(Node::Color::BLACK);
                grandParent->SetColor(Node::Color::RED);
                n = grandParent;
            }
            else
            {
                if(n == n->Parent->Left)
                {
                    n = n->Parent;
                    RotateRight(n);
                }

                n->Parent->SetColor(Node::Color::BLACK);
                grandParent->SetColor(Node::Color::RED);
                RotateLeft(grandParent);
            }
        }
    }

    root_->SetColor(Node::Color::BLACK);
}

void MMapTree::FixRemove(Node* n)
{
    while(n != root_ && n->GetColor() == Node::Color::BLACK)
    {
        if(n == n->Parent->Left)
        {
            Node* w = n->Parent->Right;

            if(w->GetColor() == Node::Color::RED)
            {
                w->SetColor(Node::Color::BLACK);
                n->Parent->SetColor(Node::Color::RED);
                RotateLeft(n->Parent);
                w = n->Parent->Right;
            }

            if(w->Left->GetColor() == Node::Color::BLACK && w->Right->GetColor() == Node::Color::BLACK)
            {
                w->SetColor(Node::Color::RED);
                n = n->Parent;
            }
            else
            {
                if(w->Right->GetColor() == Node::Color::BLACK)
                {
                    w->Left->SetColor(Node::Color::BLACK);
                    w->SetColor(Node::Color::RED);
                    RotateRight(w);
                    w = n->Parent->Right;
                }

                w->SetColor(n->Parent->GetColor());
                n->Parent->SetColor(Node::Color::BLACK);
                w->Right->SetColor(Node::Color::BLACK);
                RotateLeft(n->Parent);
                n = root_;
            }
        }
        else
        {
            Node* w = n->Parent->Left;

            if(w->GetColor() == Node::Color::RED)
            {
                w->SetColor(Node::Color::BLACK);
                n->Parent->SetColor(Node::Color::RED);
                RotateRight(n->Parent);
                w = n->Parent->Left;
            }

            if(w->Right->GetColor() == Node::Color::BLACK && w->Left->GetColor() == Node::Color::BLACK)
            {
                w->SetColor(Node::Color::RED);
                n = n->Parent;
            }
            else
            {
                if(w->Left->GetColor() == Node::Color::BLACK)
                {
                    w->Right->SetColor(Node::Color::BLACK);
                    w->SetColor(Node::Color::RED);
                    RotateLeft(w);
                    w = n->Parent->Left;
                }

                w->SetColor(n->Parent->GetColor());
                n->Parent->SetColor(Node::Color::BLACK);
                w->Left->SetColor(Node::Color::BLACK);
                RotateRight(n->Parent);
                n = root_;
            }
        }
    }

    n->SetColor(Node::Color::BLACK);
}

void MMapTree::DebugPrint(const Node* node, int level) const
{
    if(node != &nil_)
    {
        DebugPrint(node->Right, level + 1);

        for(int i = 0; i < level; ++i)
            log::Logf("\t");

        char color = (node->GetColor() == Node::Color::RED) ? 'r' : 'b';
        log::Logf("[%zx, %zx[ (%c) M %zx, O %zx\n", node->GetStart(), node->GetEnd(), color, node->MaxGap, node->GetGap());

        DebugPrint(node->Left, level + 1);
    }
}

}
