/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <arch/common/arch.hpp>
#include <arch/common/cpu.hpp>
#include <assert.h>
#include <logger.hpp>
#include <pmm.hpp>
#include <vmm.hpp>
#include <numa.hpp>
#include <spinlock.hpp>
#include <scopedlock.hpp>
#include <bitutils.hpp>
#include <compiler.hpp>
#include <panic.hpp>

#define LOG_PREFIX "PMM"

/**
 * The physical memory manager.
 * There are two allocators for the PMM.
 * 
 * The first allocator is for general purpose.
 * It runs in O(1) and can only be used to allocate and free single pages.
 * It works by keeping a singly linked list of bitmaps.
 * We always make sure the head of the linked list points to a bitmap with
 * at least one entry free.
 * Allocating and freeing is just a matter of searching a bit (happens in O(1))
 * and clearing or setting that bit.
 * 
 * The second allocator is mostly for DMA in drivers.
 * It runs in O(log(n)) and uses a buddy allocator system.
 * It has limited memory and can be used to allocate multiple contiguous pages,
 * which is a requirement for DMA.
 */
namespace mem::pmm
{

struct Entry
{
    uintptr_t Bitmap;

    /**
     * We need to remember from which domain this entry came.
     * The amount of maximum NUMA entries is limited.
     * For example if we take a look at the SLIT from ACPI, we can
     * derive that the limit is 65535.
     * That means we can reuse the "Next" pointer as a domain index.
     */
    union
    {
        Entry* Next;
        uintptr_t DomainIdx;
    };
};

/**
 * Describes a physical memory domain.
 * This is for NUMA support.
 */
struct PMMDomain
{
    Entry* Head;
    sync::Spinlock Lock;
};

static PMMDomain PMMDomains_[MAX_PROX];

constexpr auto DELTA = sizeof(uintptr_t) * 8 * PAGE_SIZE;

static uintptr_t endEntryPlacement_;

static sync::Spinlock contiguousLock_;

//////////////////////////////////////////////
//////////// CONTIGUOUS ALLOCATOR ////////////
//////////////////////////////////////////////

uintptr_t contAddress_;
uint16_t* contNodes_   = (uint16_t*)PMM_CONT_TREE;

uint16_t LeftChild(uint16_t index)
{
    return index * 2 + 1;
}

uint16_t RightChild(uint16_t index)
{
    return index * 2 + 2;
}

uint16_t Parent(uint16_t index)
{
    return ((index + 1) / 2) - 1;
}

uint16_t IsPowerOf2(uint16_t x)
{
    // Note: returns true for x = 0, but we don't care about that value
    return (x & (x - 1)) == 0;
}

uint16_t RoundSize(uint16_t x)
{
    // Casting generates better code on some platforms...
    uint32_t t = x;
    --t;
    t |= t >> 1;
    t |= t >> 2;
    t |= t >> 4;
    t |= t >> 8;
    ++t;
    return (uint16_t)t;
}

static inline uint16_t max(uint16_t a, uint16_t b)
{
    return (a > b) ? a : b;
}

uintptr_t AllocContiguousUnlocked(uint16_t size)
{
    assert(false);
    size = RoundSize(size);
    if(unlikely(size > contNodes_[0]))
    {
        LOG("Could not allocate contiguous area of %u\n", size);
        return 0;
    }

    // Choose child with smallest size which is large enough for the requested size
    uint16_t index = 0;
    uint16_t nodeSize = PMM_CONT_ROOT_SIZE;
    for(; nodeSize != size; nodeSize /= 2)
    {
        /**
         * Because of the first size check in this function,
         * we know one of the two children is big enough.
         */
        uint16_t left  = LeftChild(index);
        uint16_t right = RightChild(index);

        if(contNodes_[left] >= size)
            index = left;
        else
            index = right;
    }

    /**
     * Our offset is a multiple of nodeSize.
     * offset = nodeSize * (index in this level)
     *        = nodeSize * (index - amount of nodes before this row)
     *        = nodeSize * (index - ((PMM_CONT_ROOT_SIZE / nodeSize) - 1))
     *        = nodeSize * (index + 1) - PMM_CONT_ROOT_SIZE
     */
    uint16_t offset = nodeSize * (index + 1) - PMM_CONT_ROOT_SIZE;

    /**
     * Update values in the tree so that each node still contains the largest available
     * power of two size in their subtree.
     */
    contNodes_[index] = 0;
    while(index > 0)
    {
        index = Parent(index);
        contNodes_[index] = max(contNodes_[LeftChild(index)], contNodes_[RightChild(index)]);
    }

    return contAddress_ + offset * PAGE_SIZE;
}

uintptr_t AllocContiguous(uint16_t size)
{
    sync::ScopedLock lock(contiguousLock_);
    return AllocContiguousUnlocked(size);
}

void FreeContiguousUnlocked(uintptr_t addr)
{
    assert(false);
    assert(addr >= contAddress_ && addr < contAddress_ + PMM_CONT_ROOT_SIZE * PAGE_SIZE);
    uint16_t offset = (addr - contAddress_) / PAGE_SIZE;

    /**
     * Calculate the index in the lowest row.
     * This index will have the same memory offset.
     * We can work our way from here to the top.
     */
    uint16_t index = offset + PMM_CONT_ROOT_SIZE - 1;
    uint16_t size = 1;
    while(contNodes_[index] != 0)
    {
        index = Parent(index);
        size *= 2;
    }

    // Update values in the tree to undo the work of allocation
    contNodes_[index] = size;
    while(index > 0)
    {
        index = Parent(index);
        size *= 2;

        uint16_t leftSize = contNodes_[LeftChild(index)];
        uint16_t rightSize = contNodes_[RightChild(index)];

        if(leftSize == rightSize)
            contNodes_[index] = size;
        else
            contNodes_[index] = max(leftSize, rightSize);
    }
}

void FreeContiguous(uintptr_t addr)
{
    sync::ScopedLock lock(contiguousLock_);
    return FreeContiguousUnlocked(addr);
}

//////////////////////////////////////////////
///////////// ONE PAGE ALLOCATOR /////////////
//////////////////////////////////////////////

/**
 * Ensure the linkedlist+bitmap area exists
 * @param domain The domain
 * @param placement The placement
 */
static inline void EnsureAreaExists(PMMDomain* domain, uintptr_t placement)
{
    if(unlikely(placement >= endEntryPlacement_))
    {
        assert(endEntryPlacement_ < PMM_ENTRIES + PMM_ENTRIES_MAX_SIZE);

        vmm::Map(placement, AllocPageUnlocked(domain), vmm::MapFlags::WRITABLE
                                                        | vmm::MapFlags::NO_EXEC
                                                        | vmm::MapFlags::GLOBAL);

        endEntryPlacement_ = placement + PAGE_SIZE;
    }
}

/**
 * Calculates the bit mask so that anything in the lower range is marked used.
 * Used bits are zeroes, unused ones.
 * @param len The length
 * @return The mask
 */
static uintptr_t CalculateBitmap(uintptr_t len)
{
    return -(1ULL << (len / 0x1000));
}

/**
 * Calculates the address of a bitmap entry
 * @param paddr The physical address
 * @return The entry address
 */
static uintptr_t CalculateEntryAddress(uintptr_t paddr)
{
    return PMM_ENTRIES + (paddr / DELTA * sizeof(Entry));
}

/**
 * Align placement address down
 * @param addr The address
 * @return The aligned address
 */
static inline uintptr_t AlignPlacementDown(uintptr_t addr)
{
    return addr & -DELTA;
}

uintptr_t AllocPageUnlocked(PMMDomain* domain)
{
    assert(domain != nullptr);

    Entry* tmp = domain->Head;
    if(unlikely(!tmp))
        return 0;

    assert(tmp->Bitmap != 0);

    uint32_t bit = bitutils::LSB(tmp->Bitmap);
    tmp->Bitmap ^= (1U << bit);

	if(tmp->Bitmap == 0)
	{
		domain->Head = tmp->Next;
		tmp->DomainIdx = domain - PMMDomains_;
	}

    constexpr auto ONE_ENTRY = 8 * sizeof(uintptr_t) / sizeof(Entry);
    return (((uintptr_t)tmp - PMM_ENTRIES) * ONE_ENTRY + bit) * PAGE_SIZE;
}

uintptr_t AllocPageUnlocked()
{
    return AllocPageUnlocked(CPU_GET(MemDomain));
}

uintptr_t AllocPage(PMMDomain* domain)
{
    sync::ScopedLock lock(domain->Lock);
    return AllocPageUnlocked(domain);
}

uintptr_t AllocPage()
{
    return AllocPage(CPU_GET(MemDomain));
}

void FreePage(uintptr_t paddr)
{
    assert((paddr & PAGE_MASK) == 0);
    paddr /= PAGE_SIZE;

    Entry* base = reinterpret_cast<Entry*>(PMM_ENTRIES);
    Entry* entry = &base[paddr / sizeof(uintptr_t) / sizeof(Entry)];

	bool wasEmpty = (entry->Bitmap == 0);

	uint32_t bit = paddr % (8 * sizeof(uintptr_t));
    assert((entry->Bitmap & (1U << bit)) == 0);
    entry->Bitmap |= (1U << bit);

	if(wasEmpty && entry->DomainIdx < MAX_PROX)
	{
        PMMDomain* domain = &PMMDomains_[entry->DomainIdx];
        sync::ScopedLock lock(domain->Lock);
        entry->Next = domain->Head;
        domain->Head = entry;
	}
}

//////////////////////////////////////////////
//////////////////// INIT ////////////////////
//////////////////////////////////////////////

static unsigned int MMapCount_;
static multiboot::TagMMap* MMap_;

/**
 * Gets the MMap entry with the given index
 * @param index The index of the entry
 * @param The entry
 */
static multiboot::MMapEntry* GetMMapEntry(unsigned int index)
{
    return (multiboot::MMapEntry*)((uintptr_t)MMap_->Entries + index * MMap_->EntrySize);
}

/**
 * Fixes the mmap: fix boundaries and sort. Also maps the PMMs first entry.
 * @param kernelEnd The address at which kernel reserved memory ends
 */
static void PrepareMMap(uintptr_t kernelEnd)
{
    bool isSorted = true;
    uintptr_t lastAddress = 0;

    for(unsigned int i = 0; i < MMapCount_; ++i)
    {
        auto entry = GetMMapEntry(i);
        if(entry->Type != multiboot::MMapType::AVAILABLE)
            continue;

        #if __WORDSIZE == 32
        if(entry->Address > 0xFFFFFFFF)
        {
            entry->Type = multiboot::MMapType::RESERVED;
            continue;
        }

        if(entry->Address + entry->Length > 0xFFFFFFFF)
            entry->Length = 0x100000000 - entry->Address;
        #endif

        // Don't overwrite kernel & modules
        uintptr_t address = entry->Address;
        if(unlikely(address < kernelEnd))
        {
            uintptr_t offset = kernelEnd - address;
            if(entry->Length <= offset)
            {
                entry->Type = multiboot::MMapType::RESERVED;
                continue;
            }

            entry->Length -= offset;
            address = entry->Address = kernelEnd;
        }

        // We put the sorting check here because we don't care about unavailable areas
        if(lastAddress > address)
            isSorted = false;

        lastAddress = address;
    }

    if(!isSorted)
    {
        // There are never many entries, we can use a simple sorting algo like insertion sort
        for(unsigned int i = 1; i < MMapCount_; ++i)
        {
            auto x    = GetMMapEntry(i);
            auto addr = x->Address;
            auto len  = x->Length;
            auto type = x->Type;

            int j = i - 1;
            while(j >= 0)
            {
                auto entry = GetMMapEntry(j);
                if(entry->Address <= addr)
                    break;

                auto tmp = GetMMapEntry(j + 1);
                tmp->Address = entry->Address;
                tmp->Length = entry->Length;
                tmp->Type = entry->Type;

                j--;
            }

            x = GetMMapEntry(j + 1);
            x->Address = addr;
            x->Length = len;
            x->Type = type;
        }
    }
}

static void InitDomain(const Domain& domain, PMMDomain* pmmDomain)
{
    // We use a dummy to avoid a comparison
    Entry dummy;
    Entry* lastEntry = &dummy;

    for(const auto& range : domain.Mem)
    {
        uint64_t rangeStart = range.Address;
        uint64_t rangeEnd = range.Address + range.Length;

        for(unsigned int i = 0; i < MMapCount_; ++i)
        {
            auto entry = GetMMapEntry(i);
            if(entry->Type != multiboot::MMapType::AVAILABLE)
                continue;

            uintptr_t entryEnd = entry->Address + entry->Length;
            if(entryEnd < rangeStart || entry->Address > rangeEnd)
                break;

            uintptr_t currStart = rangeStart;
            uintptr_t currEnd = rangeEnd;

            if(currStart < entry->Address)
                currStart = entry->Address;

            if(currEnd > entry->Address + entry->Length)
                currEnd = entry->Address + entry->Length;
            
            size_t len = currEnd - currStart;

            /**
             * A memory map can contain holes, so we calculate the virtual address
             * of the entry. Later we will check if we need to map this virtual address.
             * We can use a physical frame for that mapping.
             */
            uintptr_t placement = CalculateEntryAddress(currStart);

            /**
             * To ensure a placement area exists, we need to map physical frames.
             * This is the first mapping in this domain, that means no placement area exists.
             * It also means that we cannot get physical frames yet.
             * No physical frames also means that we might not be able to map this to
             * a virtual address.
             * 
             * To solve this issue, we cheat:
             * We get some frames by shrinking this memory map entry.
             * We then give these frames to the VMM directly and it tells us how much the VMM
             * actually used.
             */
            if(unlikely(!pmmDomain->Head))
            {
                constexpr auto MAX_RESERVED_FRAMES = 2;

                // This will probably never happen
                if(unlikely(len < 2 * PAGE_SIZE))
                    continue;

                uintptr_t frames[] =
                {
                    currEnd - 1 * PAGE_SIZE,
                    currEnd - 2 * PAGE_SIZE
                };

                static_assert(sizeof(frames) / sizeof(frames[0]) == MAX_RESERVED_FRAMES);

                uintptr_t placementPage = vmm::AlignDown(placement);
                uint32_t usedFrames = vmm::MapWithDonatedFrames(placementPage, frames, vmm::MapFlags::GLOBAL
                                                                                        | vmm::MapFlags::NO_EXEC
                                                                                        | vmm::MapFlags::WRITABLE);
                currEnd -= usedFrames * PAGE_SIZE;
                len -= usedFrames * PAGE_SIZE;
                endEntryPlacement_ = placementPage + PAGE_SIZE;
                pmmDomain->Head = reinterpret_cast<Entry*>(placement);
            }

            /**
             * Helper lambda to add an entry at the current placement address
             */
            auto AddEntry = [&](uintptr_t bitmap)
            {
                assert(bitmap != 0);
                EnsureAreaExists(pmmDomain, placement);
                Entry* entry = reinterpret_cast<Entry*>(placement);
                entry->Bitmap = bitmap;
                lastEntry->Next = entry;
                lastEntry = entry;
                placement += sizeof(Entry);
            };

            // Length offset
            uintptr_t current = 0;

            // Begin must be aligned
            if(currStart % DELTA)
            {
                uintptr_t lower = AlignPlacementDown(currStart);
                uintptr_t bitmap = CalculateBitmap(currStart - lower);

                // It may be possible that this length does not cover a whole entry
                if(unlikely(len < DELTA))
                    bitmap &= ~CalculateBitmap(currEnd - lower);

                /**
                 * It is possible that entries overlap in a DELTA page range
                 * The overlapping can only happen in the first entry.
                 */
                if(unlikely(placement == (uintptr_t)lastEntry))
                {
                    lastEntry->Bitmap |= bitmap;
                    placement += sizeof(Entry);
                }
                else
                    AddEntry(bitmap);

                current = DELTA - (currStart - lower);
            }

            // Loop through in complete chunks
            for(; current + DELTA < len; current += DELTA)
                AddEntry(UINTPTR_MAX);

            // Maybe incomplete end
            if(current < len)
                AddEntry(~CalculateBitmap(len - current));

            lastEntry->Next = nullptr;
        }
    }
}

static void SetupContiguousAllocator()
{
    // TODO
    // Map contiguous allocator tree
    /*for(uintptr_t x = 0; x < PMM_CONT_TREE_PAGES; ++x)
        vmm::Map(PMM_CONT_TREE + x, AllocPageUnlocked(), vmm::MapFlags::WRITABLE | vmm::MapFlags::NO_EXEC | vmm::MapFlags::GLOBAL);

    // Initialize contiguous allocator tree (full binary tree)
    uint16_t nodeSize = PMM_CONT_ROOT_SIZE * 2;
    for(uint16_t i = 0; i < PMM_CONT_NODE_COUNT; ++i)
    {
        if(IsPowerOf2(i + 1))
            nodeSize /= 2;
        
        contNodes_[i] = nodeSize;
    }*/
}

void Init(multiboot::TagMMap* mmap, uintptr_t kernelEnd)
{
    // Early setup
    MMapCount_ = (mmap->Header.Size - offsetof(multiboot::TagMMap, Entries)) / mmap->EntrySize;
    MMap_ = mmap;
    PrepareMMap(kernelEnd);

    // Setup PMM domains
    auto memDomains = mem::GetMemDomains();
    if(!memDomains)
    {
        // No memory domains, so make one big domain that captures everything
        Domain domain;
        domain.Mem.EmplaceBack(0x00, UINTPTR_MAX);
        InitDomain(domain, &PMMDomains_[0]);
        CPU_SET(MemDomain, &PMMDomains_[0]);
    }
    else
    {
        auto pmmDomain = &PMMDomains_[0];
        for(const auto& numaDomain : memDomains->Domains)
        {
            InitDomain(numaDomain, pmmDomain);
            if(numaDomain.Id == arch::GetBSPDomain())
                CPU_SET(MemDomain, pmmDomain);
            
            ++pmmDomain;
        }
    }

    // TODO: toch geent ijdelijke CPU struct gebruiken maar gewoon direct allocaten zonder heap?
}

}
