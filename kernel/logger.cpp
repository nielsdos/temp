/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdarg.h>
#include <stdio.h>
#include <logger.hpp>
#include <arch/x86-common/vgascreenlogger.hpp>
#include <lfbscreenlogger.hpp>
#include <spinlock.hpp>

namespace log
{

// The lock will never be held for long
sync::Spinlock Lock_;

// Use a temp early logger if in debug mode so we can see early messages
#ifndef NDEBUG
VGAScreenLogger tmpLogger_;
Logger* logger_ = &tmpLogger_;
#else
Logger* logger_ = nullptr;
#endif

Logger::~Logger()
{
}

void Logger::Write(const char* buffer, size_t size)
{
    for(size_t i = 0; i < size; ++i)
        PutChar(buffer[i]);
}

void InitScreenLogger(const multiboot::TagLFB* lfb)
{
    if(lfb->Type != multiboot::LFBType::EGA_TEXT)
        SetLogger(new LFBScreenLogger(lfb));
    else
        SetLogger(new VGAScreenLogger());
}

void SetLogger(Logger* logger)
{
    Lock_.Lock();

    auto old = logger_;
    logger_ = logger;

    Lock_.Unlock();

    if(old)
        delete old;
}

int VLogf(const char* format, va_list list)
{
    if(!logger_)
        return -1;

    char buffer[256];
    int ret = vsnprintf(buffer, sizeof(buffer), format, list);
    if(ret > 0)
    {
        Lock_.Lock();
        logger_->Write(buffer, ret);
        Lock_.Unlock();
    }

    return ret;
}

int Logf(const char* __restrict format, ...)
{
    va_list list;
    va_start(list, format);
    int ret = VLogf(format, list);
    va_end(list);
    return ret;
}

}
