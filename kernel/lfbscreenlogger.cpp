/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>
#include <font.hpp>
#include <vmm.hpp>
#include <lfbscreenlogger.hpp>
#include <process.hpp>

namespace log
{

LFBScreenLogger::LFBScreenLogger(const multiboot::TagLFB* tag)
{
    // TODO: assumes 32bpp XRGB format
    bpp_    = tag->Depth / 8;
    pitch_  = tag->Pitch / bpp_;
    width_  = tag->Width / 8;
    height_ = tag->Height / 16;

    using namespace mem::vmm;
    vidMem_ = (uint32_t*)tasking::GetKernelProcess()->AddMapping(tag->Address, CalcSize(),
                    MapFlags::WRITABLE | MapFlags::NO_EXEC | MapFlags::CACHE_WRITECOMBINE);
    //Clear();
}

LFBScreenLogger::~LFBScreenLogger()
{
    tasking::GetKernelProcess()->RemoveMapping((uintptr_t)vidMem_, CalcSize());
}

void LFBScreenLogger::Clear() const
{
    memset(vidMem_, 0, CalcSize());
}

void LFBScreenLogger::RenderChar(char c) const
{
    if(c < 32)
        c = 32;

    unsigned char* row = FontData_[(int)c - 32];

    constexpr uint32_t color = 0xFFC0C0C0;
    int ox = cursorX_ * 8;
    int oy = cursorY_ * 16;

    for(int y = 0; y < 16; ++y)
    {
        if(row[y] & 0x80) vidMem_[(oy + y) * pitch_ + (ox + 0)] = color;
        if(row[y] & 0x40) vidMem_[(oy + y) * pitch_ + (ox + 1)] = color;
        if(row[y] & 0x20) vidMem_[(oy + y) * pitch_ + (ox + 2)] = color;
        if(row[y] & 0x10) vidMem_[(oy + y) * pitch_ + (ox + 3)] = color;
        if(row[y] & 0x08) vidMem_[(oy + y) * pitch_ + (ox + 4)] = color;
        if(row[y] & 0x04) vidMem_[(oy + y) * pitch_ + (ox + 5)] = color;
        if(row[y] & 0x02) vidMem_[(oy + y) * pitch_ + (ox + 6)] = color;
        if(row[y] & 0x01) vidMem_[(oy + y) * pitch_ + (ox + 7)] = color;
    }
}

void LFBScreenLogger::Scroll() const
{
    // Using memcpy for this is faster than memmove
    for(uint16_t i = 1; i < height_; ++i)
        memcpy(vidMem_ + pitch_ * ((i - 1) * 16), vidMem_ + pitch_ * (i * 16), pitch_ * 16 * bpp_);

    memset(vidMem_ + pitch_ * ((height_ * 16) - 16), 0, pitch_ * 16 * bpp_);
}

size_t LFBScreenLogger::CalcSize() const
{
    return pitch_ * (height_ * 16) * bpp_;
}

}
