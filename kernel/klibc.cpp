/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <bits/wordsize.h>
#include <sys/types.h>
#include <cstdint>
#include <stddef.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <compiler.hpp>

// Used for fast checking if a number has a zero byte
#if __WORDSIZE == 32
#define MASK01 0x01010101UL
#define MASK80 0x80808080UL
#elif __WORDSIZE == 64
#define MASK01 0x0101010101010101ULL
#define MASK80 0x8080808080808080ULL
#endif
#define HAS_ZERO(v) (((v) - MASK01) & ~(v) & MASK80)

constexpr size_t PTR_MASK = sizeof(size_t) - 1;

extern "C"
{

void* memmove(void* dst, const void* src, size_t n)
{
    uint8_t* dst8 = (uint8_t*)dst;
    const uint8_t* src8 = (const uint8_t*)src;

    if(src8 + n <= dst8 || dst8 + n <= src8)
        return memcpy(dst, src, n);

    if(dst8 < src8)
    {
        for(size_t i = 0; i < n; ++i)
            dst8[i] = src8[i];
    }
    else
    {
        for(size_t i = 0; i < n; ++i)
            dst8[i] = src8[i];
    }

    return dst;
}

void* memcpy(void* __restrict dst, const void* __restrict src, size_t n)
{
    uintptr_t dstRemain = (uintptr_t)dst & PTR_MASK;

    /**
     * Can't use fast copy because alignment remainder is not the same.
     * Also use this for small memcpy's, since this isn't common, we use "unlikely".
     */
    if(n < 16 || dstRemain != ((uintptr_t)src & PTR_MASK))
    {
        char*  dstPtr = (char* )dst;
        const char* srcPtr = (const char*)src;
        for(size_t i = 0; i < n; ++i)
            dstPtr[i] = srcPtr[i];

        return dst;
    }

    union
    {
        const uint8_t*  __restrict src8;
        const uint16_t* __restrict src16;
        const uint32_t* __restrict src32;
        const size_t*   __restrict srcz;
    } s
    {
        .src8 = (const uint8_t*)src
    };

    union
    {
        uint8_t*  __restrict dst8;
        uint16_t* __restrict dst16;
        uint32_t* __restrict dst32;
        size_t*   __restrict dstz;
    } d
    {
        .dst8 = (uint8_t*)dst
    };

    // Align
    if(dstRemain)
    {
        // Remainder is at most 7 on 64bit and 3 on 32bit
        if(dstRemain & 1)
        {
            n--;
            *d.dst8++ = *s.src8++;
        }

        if(dstRemain & 2)
        {
            n -= 2;
            *d.dst16++ = *s.src16++;
        }

        #if __WORDSIZE == 64
        if(dstRemain & 4)
        {
            n -= 4;
            *d.dst32++ = *s.src32++;
        }
        #endif
    }

    // Fast copy
    size_t num = n / sizeof(size_t);
    while(num--)
        *d.dstz++ = *s.srcz++;

    // Copy remaining bytes
    n -= num * sizeof(size_t);
    if(n)
    {
        // Remainder is at most 7 on 64bit and 3 on 32bit
        if(n & 1)
            *d.dst8++ = *s.src8++;

        if(n & 2)
            *d.dst16++ = *s.src16++;

        #if __WORDSIZE == 64
        if(n & 4)
            *d.dst32++ = *s.src32++;
        #endif
    }

    return dst;
}

void* memset(void* dst, int value, size_t length)
{
    uint8_t* dst8 = (uint8_t*)dst;
    if(((uintptr_t)dst & PTR_MASK) == 0)
    {
        size_t* dstz = (size_t*)dst;

        #if __WORDSIZE == 32
        size_t val = (size_t)((uint8_t)value) << 0
                        | (size_t)((uint8_t)value) << 8
                        | (size_t)((uint8_t)value) << 16
                        | (size_t)((uint8_t)value) << 24;
        #elif __WORDSIZE == 64
        size_t val = (size_t)((uint8_t)value) << 0
                        | (size_t)((uint8_t)value) << 8
                        | (size_t)((uint8_t)value) << 16
                        | (size_t)((uint8_t)value) << 24
                        | (size_t)((uint8_t)value) << 32
                        | (size_t)((uint8_t)value) << 40
                        | (size_t)((uint8_t)value) << 48
                        | (size_t)((uint8_t)value) << 56;
        #endif

        while(length >= sizeof(size_t))
        {
            *dstz++ = val;
            length -= sizeof(size_t);
        }

        dst8 = (uint8_t*)dstz;
    }

    while(length-- > 0)
        *dst8++ = (uint8_t)value;

    return dst;
}

size_t strlen(const char* str)
{
    // Align str
    const char* x = str;
    for(; ((uintptr_t)x & PTR_MASK); ++x)
    {
        // Zero already found
        if(*x == '\0')
            return (x - str);
    }

    /* 
     * Use a bit trick to find the zero byte fast
     * Note: yes, it's possible read beyond the string length, but it doesn't matter
     */
    const size_t* y = (const size_t*)x;
    while(!HAS_ZERO(*y))
        ++y;

    // Check which one had a zero
    x = (const char*)y;
    if(x[0] == '\0') return x - str + 0;
    if(x[1] == '\0') return x - str + 1;
    if(x[2] == '\0') return x - str + 2;
    if(x[3] == '\0') return x - str + 3;
    #if __WORDSIZE == 64
    if(x[4] == '\0') return x - str + 4;
    if(x[5] == '\0') return x - str + 5;
    if(x[6] == '\0') return x - str + 6;
    if(x[7] == '\0') return x - str + 7;
    #endif

    __builtin_unreachable();
}

int memcmp(const void* s1, const void* s2, size_t n)
{
    const int8_t* a = (const int8_t*)s1;
    const int8_t* b = (const int8_t*)s2;
    for(size_t i = 0; i < n; ++i)
    {
        int8_t aa = a[i];
        int8_t bb = b[i];
        if(aa < bb)
            return -1;
        if(aa > bb)
            return 1;
    }

    return 0;
}

int strcmp(const char* s1, const char* s2)
{
    size_t i = 0;
    while(true)
    {
        char aa = s1[i];
        char bb = s2[i];

        if(aa == '\0' && bb == '\0')
            return 0;
        if(aa < bb)
            return -1;
        if(aa > bb)
            return 1;
        
        ++i;
    }
}

int strncmp(const char* s1, const char* s2, size_t n)
{
    for(size_t i = 0; i < n; ++i)
    {
        char aa = s1[i];
        char bb = s2[i];

        if(aa == '\0' && bb == '\0')
            return 0;
        if(aa < bb)
            return -1;
        if(aa > bb)
            return 1;
    }

    return 0;
}

const char* strchr(const char* s, int c)
{
    c = (char)c;

    // Align s
    const char* x = s;
    for(; ((uintptr_t)x & PTR_MASK); ++x)
    {
        // Already found
        if(*x == c)
            return (char*)x;
        else if(*x == '\0')
            return nullptr;
    }

    /*
     * Fast search: zero the search part and search for zero.
     * Uses the same bit trick as in strlen.
     */
    const size_t pattern = MASK01 * c;
    const size_t* y = (const size_t*)x;
    while(!HAS_ZERO(*y) && !HAS_ZERO(*y ^ pattern))
        ++y;

    // Check which one had the pattern
    x = (const char*)y;
    if(x[0] == c) return (char*)x + 0;
    if(x[1] == c) return (char*)x + 1;
    if(x[2] == c) return (char*)x + 2;
    if(x[3] == c) return (char*)x + 3;
    #if __WORDSIZE == 64
    if(x[4] == c) return (char*)x + 4;
    if(x[5] == c) return (char*)x + 5;
    if(x[6] == c) return (char*)x + 6;
    if(x[7] == c) return (char*)x + 7;
    #endif

    return nullptr;
}

char* strcpy(char* __restrict dst, const char* __restrict src)
{
    // Use fast copy if alignment remainder is the same
    if(((uintptr_t)dst & PTR_MASK) == ((uintptr_t)src & PTR_MASK))
    {
        // Align and copy unaligned parts
        for(; ((uintptr_t)src & PTR_MASK); ++src, ++dst)
        {
            if((*dst = *src) == '\0')
                return dst;
        }

        // Fast copy remaining aligned parts
        size_t* __restrict d = (size_t* __restrict)dst;
        const size_t* __restrict s = (const size_t* __restrict)src;
        while(!HAS_ZERO(*s))
            *d++ = *s++;

        dst = (char* __restrict)d;
        src = (const char* __restrict)s;
    }

    // Copy remaining bytes
    while((*dst++ = *src++) != '\0');

    return dst;
}

/**
 * Prints a string to a buffer
 * @param s[out] The buffer
 * @param src The source
 * @param len The length of the source
 * @param n[out] The amount of bytes left in the buffer
 */
static void writeToBuffer(char* __restrict& s, const char* __restrict src, size_t len, size_t& n)
{
    size_t remaining = len;
    if(n < len)
        remaining = n;

    if(remaining > 0)
    {
        memcpy(s, src, remaining);
        s += remaining;
        n -= remaining;
    }
}

[[gnu::format(printf, 3, 0)]]
int vsnprintf(char* __restrict s, size_t n, const char* __restrict format, va_list list)
{
    int total = 0;

    enum class Length
    {
        DEFAULT,
        hh,
        h,
        l,
        ll,
        z,
        j,
        t
    };

    enum
    {
        TYPE_DEFAULT = 0,
        TYPE_LEFT_ALIGN = 1,
        TYPE_ZERO = 2
    };

    #define INC_FORMAT  { \
                            ++format; \
                            if(!*format) break; \
                        }

    #define PADDING_RIGHT(type, len) if(!(type & TYPE_LEFT_ALIGN)) PaddingCommon(type, len);
    #define PADDING_LEFT(type, len) if(type & TYPE_LEFT_ALIGN) PaddingCommon(type, len);

    auto PutOne = [&n, &s, &total](char c)
    {
        if(unlikely(n > 0))
        { 
            --n;
            *s++ = c;
        }

        ++total;
    };

    auto GetNum = [&format](size_t& v)
    {
        while(isdigit(*format))
        {
            v = (v * 10) + (*format - '0');
            INC_FORMAT
        }
    };

    if(unlikely(n == 0))
        return -1;

    --n;

    /**
     * We want to return the total bytes it would take to completely write the resulting string,
     * not the actual amount of written bytes.
     */
    while(*format)
    {
        // Formatted part
        if(*format == '%')
        {
            INC_FORMAT

            // Type
            int type = TYPE_DEFAULT;
            while(true)
            {
                if(*format == '-')
                    type |= TYPE_LEFT_ALIGN;
                else if(*format == '0')
                    type |= TYPE_ZERO;
                else
                    break;

                INC_FORMAT
            }

            // Width
            size_t width = 0;
            GetNum(width);

            auto PaddingCommon = [&](int type, size_t len)
            {
                char widthFill = (type & TYPE_ZERO) ? '0' : ' ';
                while(len < width)
                {
                    PutOne(widthFill);
                    --width;
                }
            };
            
            // Precision
            size_t precision = 0;
            if(*format == '.')
            {
                INC_FORMAT
                GetNum(precision);
            }

            // Length specifiers
            Length lengthSpecifier = Length::DEFAULT;
            switch(*format)
            {
                case 'l':
                {
                    INC_FORMAT

                    lengthSpecifier = Length::l;
                    if(*format == 'l')
                    {
                        lengthSpecifier = Length::ll;
                        ++format;
                    }

                    break;
                }

                case 'h':
                {
                    INC_FORMAT

                    lengthSpecifier = Length::h;
                    if(*format == 'h')
                    {
                        lengthSpecifier = Length::hh;
                        ++format;
                    }

                    break;
                }

                case 'z':
                {
                    ++format;
                    lengthSpecifier = Length::z;
                    break;
                }

                case 'j':
                {
                    ++format;
                    lengthSpecifier = Length::j;
                    break;
                }

                case 't':
                {
                    ++format;
                    lengthSpecifier = Length::t;
                    break;
                }
            }

            // Format specifier
            switch(*format)
            {
                case 's':
                {
                    const char* str = va_arg(list, const char*);
                    if(!str)
                        str = "(null)";

                    size_t size = (precision > 0) ? precision : strlen(str);

                    PADDING_RIGHT(type, size)
                    writeToBuffer(s, str, size, n);
                    total += size;
                    PADDING_LEFT(type, size)

                    break;
                }

                case 'c':
                {
                    char ch = va_arg(list, int /* promoted to int */);
                    PADDING_RIGHT(type, 1)
                    PutOne(ch);
                    PADDING_LEFT(type, 1)
                    break;
                }

                case '%':
                {
                    PutOne('%');
                    break;
                }

                case 'p':
                case 'x':
                case 'X':
                case 'u':
                case 'd':
                case 'o':
                {
                    char specifier = *format;

                    // Process 'p': transforms to 'x'
                    if(specifier == 'p')
                    {
                        specifier = 'x';
                        PutOne('0');
                        PutOne('x');
                    }

                    // Lookup table for numbers
                    const char* numsLower = "0123456789abcdef";
                    const char* numsUpper = "0123456789ABCDEF";
                    const char* numsTable = (specifier == 'X') ? numsUpper : numsLower;

                    int base;
                    if(specifier == 'd' || specifier == 'u')
                        base = 10;
                    else if(specifier == 'o')
                        base = 8;
                    else
                        base = 16;

                    // Length specifier
                    uintmax_t value;
                    if(specifier == 'd' || specifier == 'o')
                    {
                        switch(lengthSpecifier)
                        {
                            default:
                                value = va_arg(list, int);
                                break;

                            case Length::hh:
                                value = (char)va_arg(list, int);
                                break;

                            case Length::h:
                                value = (short)va_arg(list, int);
                                break;

                            case Length::l:
                                value = va_arg(list, long);
                                break;

                            case Length::ll:
                                value = va_arg(list, long long);
                                break;

                            case Length::z:
                                value = va_arg(list, ssize_t);
                                break;

                            case Length::j:
                                value = va_arg(list, intmax_t);
                                break;

                            case Length::t:
                                value = va_arg(list, ptrdiff_t);
                                break;
                        }

                        if((intmax_t)value < 0)
                        {
                            value = (uintmax_t)(-(intmax_t)value);
                            PutOne('-');
                        }
                    }
                    else
                    {
                        switch(lengthSpecifier)
                        {
                            default:
                                value = va_arg(list, unsigned int);
                                break;

                            case Length::hh:
                                value = (unsigned char)va_arg(list, unsigned int);
                                break;

                            case Length::h:
                                value = (unsigned short)va_arg(list, unsigned int);
                                break;

                            case Length::l:
                                value = va_arg(list, unsigned long);
                                break;

                            case Length::ll:
                                value = va_arg(list, unsigned long long);
                                break;

                            case Length::z:
                                value = va_arg(list, size_t);
                                break;

                            case Length::j:
                                value = va_arg(list, uintmax_t);
                                break;

                            case Length::t:
                                value = va_arg(list, ptrdiff_t);
                                break;
                        }
                    }

                    // Find length of number
                    uint8_t len = 0;
                    uintmax_t tmp = value;
                    do
                    {
                        tmp /= base;
                        ++len;
                    }
                    while(tmp != 0);

                    if(precision > width)
                        width = 0;
                    else
                        width -= precision;

                    // Precision padding for number
                    while(len < precision)
                    {
                        PutOne('0');
                        --precision;
                    }

                    PADDING_RIGHT(type, len)

                    char buf[24];
                    for(int x = len - 1; x >= 0; --x)
                    {
                        buf[x] = numsTable[value % base];
                        value /= base;
                    }

                    writeToBuffer(s, buf, len, n);
                    total += len;

                    PADDING_LEFT(type, len)

                    break;
                }

                default:
                    return -1;
            }
        }
        // Regular character
        else
        {
            PutOne(*format);
        }

        ++format;
    }

    *s = '\0';

    #undef INC_FORMAT
    #undef PADDING_RIGHT
    #undef PADDING_LEFT

    return total;
}

int snprintf(char* __restrict s, size_t n, const char* __restrict format, ...)
{
    va_list list;
    va_start(list, format);
    int ret = vsnprintf(s, n, format, list);
    va_end(list);
    return ret;
}

int isdigit(int c)
{
    return ((unsigned int)c - '0') < 10;
}

}
