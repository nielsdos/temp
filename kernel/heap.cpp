/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <logger.hpp>
#include <vmm.hpp>
#include <pmm.hpp>
#include <heap.hpp>
#include <spinlock.hpp>
#include <scopedlock.hpp>
#include <bitutils.hpp>
#include <compiler.hpp>

#define LOG_PREFIX "HEAP"

// Debug flags
#define DUMP_BLOCK_INFO false
#define LOG_MERGES      false

namespace mem::heap
{

struct BlockHead;

struct BlockEnd
{
    uint32_t Size;

    inline BlockHead* GetHead() const
    {
        return (BlockHead*)((uintptr_t)this + sizeof(BlockEnd) - Size);
    }
};

constexpr uint32_t FLAG_USED = 0b001;
constexpr uint32_t FLAG_MASK = 0b111;

struct BlockHead
{
    // Note: size is at least a multiple of 8 (even 16 on 64-bit), so we have the 3 lower bits free
    uint32_t SizeAndFlags;

    /**
     * If the block is free, it is stored in a doubly linked list.
     * This means we need to keep track of two pointers:
     *   - the "next item" ptr
     *   - the "previous item" ptr
     *
     * When the block is free, we can store this in the data area if the size of the block is big enough.
     * Therefor, we make sure the minimal allocation size can handle these pointers
     */

    inline BlockHead** GetPrevFreePtr() const
    {
        return (BlockHead**)((uintptr_t)GetData() + sizeof(BlockHead*));
    }

    inline BlockHead** GetNextFreePtr() const
    {
        return (BlockHead**)GetData();
    }

    inline BlockEnd* GetEnd() const
    {
        return (BlockEnd*)((uintptr_t)this + SizeAndFlags - sizeof(BlockEnd));
    }

    inline bool IsUsed() const
    {
        return (SizeAndFlags & FLAG_USED);
    }

    inline void* GetData() const
    {
        return (void*)((uintptr_t)this + sizeof(BlockHead));
    }

    inline BlockHead* GetLeft() const
    {
        BlockEnd* previousEnd = (BlockEnd*)((uintptr_t)this - sizeof(BlockEnd));
        return previousEnd->GetHead();
    }

    inline BlockHead* GetRight() const
    {
        return (BlockHead*)((uintptr_t)this + SizeAndFlags);
    }
};

#if __WORDSIZE == 32
constexpr size_t HEAP_ALIGNMENT = 8;
#elif __WORDSIZE == 64
constexpr size_t HEAP_ALIGNMENT = 16;
#endif

/**
 * Note: the heap pointer should be aligned to the first block, if we keep alignment
 * it means every allocation will be aligned.
 */
constexpr size_t ALIGN_OFF = HEAP_ALIGNMENT - sizeof(BlockHead);
constexpr size_t OVERHEAD  = sizeof(BlockHead) + sizeof(BlockEnd);
constexpr size_t MIN_ALLOC = 32;
constexpr size_t NUM_BINS  = 16;

BlockHead* bins_[NUM_BINS];

/**
 * If bit x is set, it means there is at least one block free
 * in the bin for size 2**x.
 * We do this so we can easily find a large enough bin with
 * at least one free block.
 */
uint32_t binMask_;

sync::Spinlock heapLock_;
uintptr_t currentHeapPtr_;
uintptr_t heapStartPtr_;
uintptr_t currentHeapEnd_;

#if DUMP_BLOCK_INFO
/**
 * Dumps info about a block
 * @param block The block head
 */
static void DumpBlockInfo(BlockHead* block)
{
    LOG("Block info for %p\n  Head->SizeAndFlags = %u\n  NextFree = %p\n  End->Size = %u\n",
        block, block->SizeAndFlags, *block->GetNextFreePtr(), block->GetEnd()->Size);
}
#endif

/**
 * Gets the amount of unused space left in the heap
 * @return The amount of unused space
 */
static inline uintptr_t GetUnusedSpace()
{
    return currentHeapEnd_ - currentHeapPtr_;
}

/**
 * Gets the head of a block using the data address
 * @param addr The address of the data
 * @return The head
 */
static inline BlockHead* GetHead(uintptr_t addr)
{
    return reinterpret_cast<BlockHead*>(addr - sizeof(BlockHead));
}

static inline BlockHead* GetHead(void* addr)
{
    return GetHead(reinterpret_cast<uintptr_t>(addr));
}

static void PrintFreeList(uint32_t bin)
{
    assert(bin < NUM_BINS);

    LOG("Free list of bin %u: START", bin);

    BlockHead* head = bins_[bin];
    BlockHead* prev = nullptr;
    while(head)
    {
        char ch = (*head->GetPrevFreePtr() == prev) ? '-' : '~';
        LOG("%c%p(%u)", ch, head, head->SizeAndFlags & ~FLAG_MASK);
        if(ch == '~')
            LOG("[!prev=%p]", *head->GetPrevFreePtr());

        prev = head;
        head = *head->GetNextFreePtr();
    }

    LOG("\n");
}

void PrintAllFreeLists()
{
    for(uint32_t i = 5; i < NUM_BINS; ++i)
        PrintFreeList(i);
}

/**
 * Get upper bin size ceil(log2)
 * @param size The size
 * @return The upper bin size
 */
static inline uint32_t GetBinOfSizeUpper(uint32_t size)
{
    assert(size != 0);
    uint32_t smallestBin = bitutils::MSB(size - 1) + 1;
    assert(smallestBin < NUM_BINS);
    return smallestBin;
}

/**
 * Get lower bin size floor(log2)
 * @param size The size
 * @return The lower bin size
 */
static inline uint32_t GetBinOfSizeLower(uint32_t size)
{
    assert(size != 0);
    uint32_t smallestBin = bitutils::MSB(size);
    assert(smallestBin < NUM_BINS);
    return smallestBin;
}

/**
 * Adds a block to the corresponding bin
 * @param block The block
 */
static void AddBlock(BlockHead* block)
{
    #if DUMP_BLOCK_INFO
    LOG("AddBlock: %p\n", block);
    DumpBlockInfo(block);
    #endif

    assert(block->SizeAndFlags >= MIN_ALLOC);

    uint32_t smallestBin = GetBinOfSizeLower(block->SizeAndFlags);

    if(bins_[smallestBin])
        *bins_[smallestBin]->GetPrevFreePtr() = block;

    *block->GetNextFreePtr() = bins_[smallestBin];
    *block->GetPrevFreePtr() = nullptr;

    bins_[smallestBin] = block;
    binMask_ |= (1U << smallestBin);
}

/**
 * Removes a block from the bin
 * @param block The block
 */
static void RemoveBlock(BlockHead* block)
{
    #if DUMP_BLOCK_INFO
    LOG("RemoveBlock: %p\n", block);
    DumpBlockInfo(block);
    #endif

    BlockHead* next = *block->GetNextFreePtr();
    BlockHead* prev = *block->GetPrevFreePtr();

    if(next)
        *next->GetPrevFreePtr() = prev;

    if(prev)
        *prev->GetNextFreePtr() = next;
    else
    {
        uint32_t smallestBin = GetBinOfSizeLower(block->SizeAndFlags);
        bins_[smallestBin] = next;
        if(!next)
            binMask_ ^= (1U << smallestBin);
    }
}

/**
 * Expands the heap
 * @param growSize How much the heap should grow
 * @return True if successful
 */
static bool ExpandHeap(uint32_t growSize)
{
    assert(growSize != 0);
    assert((growSize & PAGE_MASK) == 0);

    if(unlikely(currentHeapEnd_ + PAGE_SIZE >= HEAP_MAX_END))
        return false;

    /**
     * If we can't allocate all, then just leave the already allocated memory.
     * It would be used later anyway.
     */
    while(growSize > 0)
    {
        uintptr_t phys = pmm::AllocPage();
        if(unlikely(phys == 0))
            return false;

        if(unlikely(!vmm::Map(currentHeapEnd_, phys, vmm::MapFlags::WRITABLE | vmm::MapFlags::NO_EXEC | vmm::MapFlags::GLOBAL)))
            return false;

        currentHeapEnd_ += PAGE_SIZE;
        growSize -= PAGE_SIZE;
    }

    return true;
}

void Init()
{
    heapStartPtr_   = HEAP_START + ALIGN_OFF;
    currentHeapPtr_ = heapStartPtr_;
    currentHeapEnd_ = heapStartPtr_ - ALIGN_OFF;

    ExpandHeap(mem::PAGE_SIZE);
}

extern "C"
[[gnu::malloc]]
void* aligned_alloc(size_t alignment, size_t size)
{
    assert(alignment % HEAP_ALIGNMENT == 0);

    if(unlikely(alignment <= HEAP_ALIGNMENT))
        return malloc(size);

    void* mem = malloc(size + (alignment - 1));
    if(unlikely(!mem))
        return nullptr;

    uintptr_t next = ((uintptr_t)mem + (alignment - 1)) & -alignment;
    if((uintptr_t)mem == next)
        return mem;

    // Split allocation in two blocks
    BlockHead* firstBlock = GetHead(mem);
    BlockHead* nextBlock = GetHead(next);

    uintptr_t firstSize = (uintptr_t)nextBlock - (uintptr_t)firstBlock;

    nextBlock->SizeAndFlags = (firstBlock->SizeAndFlags & ~FLAG_MASK) - firstSize;
    nextBlock->GetEnd()->Size = nextBlock->SizeAndFlags;
    nextBlock->SizeAndFlags |= FLAG_USED;
    firstBlock->SizeAndFlags = firstSize;
    firstBlock->GetEnd()->Size = firstSize;
    AddBlock(firstBlock);

    return (void*)next;
}

extern "C"
[[gnu::malloc]]
void* malloc(size_t size)
{
    assert(size != 0);
    assert(size < (1ULL << NUM_BINS));

    // Overhead and alignment
    size = (size + OVERHEAD + (HEAP_ALIGNMENT - 1)) & -HEAP_ALIGNMENT;
    if(size < MIN_ALLOC)
        size = MIN_ALLOC;

    sync::ScopedLock lock(heapLock_);

    /**
     * Find first available large enough bin
     * 
     * smallestBin is the index of the first bin that holds blocks that are large enough
     * to fit *size* bytes in it.
     * But that does not mean that there are free blocks.
     * 
     * So we calculate a mask to mask out all bins that are too small.
     * Then we find the first bit set, that will be the first bin that is big enough and
     * contains at least one free block.
     * 
     * 
     * Example:
     * 
     * size = 48
     * smallestBin = 6
     * So that means bin 6 is the first bin which holds blocks that are big enough.
     * We now need to mask off everything that is too small.
     * Our mask needs to mask of the lower 6 bits.
     */
    uint32_t smallestBin = GetBinOfSizeUpper(size);
    uint32_t firstBin    = bitutils::LSB(binMask_ & -(1U << smallestBin));

    // Nothing free
    if(firstBin == 0)
    {
        // If we have not enough space left, try to allocate more
        if(GetUnusedSpace() < size && unlikely(!ExpandHeap(mem::vmm::AlignUp(size))))
            return nullptr;

        BlockHead* head = reinterpret_cast<BlockHead*>(currentHeapPtr_);
        head->SizeAndFlags = size;
        BlockEnd* end = head->GetEnd();
        end->Size = size;
        head->SizeAndFlags |= FLAG_USED;

        currentHeapPtr_ += size;

        return head->GetData();
    }
    // Bin with free stuff available
    else
    {
        BlockHead* head = bins_[firstBin];
        assert(head->SizeAndFlags >= size);
        BlockHead* next = *head->GetNextFreePtr();

        bins_[firstBin] = next;
        if(!next)
            binMask_ ^= (1U << firstBin);
        else
            *next->GetPrevFreePtr() = nullptr;

        // Split if it's worth it
        uint32_t remaining = head->SizeAndFlags - size;
        if(remaining >= OVERHEAD + MIN_ALLOC)
        {
            // Keep left, free right
            head->SizeAndFlags = size;
            BlockEnd* leftEnd = head->GetEnd();
            leftEnd->Size = size;

            BlockHead* rightHead = head->GetRight();
            rightHead->SizeAndFlags = remaining;

            BlockEnd* rightEnd = rightHead->GetEnd();
            rightEnd->Size = remaining;

            AddBlock(rightHead);
        }

        head->SizeAndFlags |= FLAG_USED;
        return head->GetData();
    }

    return nullptr;
}

extern "C"
void free(void* addr)
{
    assert(addr != nullptr);
    assert((uintptr_t)addr >= heapStartPtr_ && (uintptr_t)addr < currentHeapPtr_);

    BlockHead* head = GetHead(addr);
    head->SizeAndFlags ^= FLAG_USED;

    sync::ScopedLock lock(heapLock_);

    // Left merge
    if(likely((uintptr_t)head > heapStartPtr_))
    {
        BlockHead* leftHead = head->GetLeft();

        if(!leftHead->IsUsed())
        {
            #if LOG_MERGES
            LOG("Merging left: %p merging with %p\n", head, leftHead);
            #endif

            RemoveBlock(leftHead);

            leftHead->SizeAndFlags += head->SizeAndFlags;
            BlockEnd* leftEnd = leftHead->GetEnd();
            leftEnd->Size = leftHead->SizeAndFlags;
            head = leftHead;
        }
    }

    // Right merge
    BlockHead* rightHead = head->GetRight();
    if(likely((uintptr_t)rightHead < currentHeapPtr_))
    {
        if(!rightHead->IsUsed())
        {
            #if LOG_MERGES
            LOG("Merging right: %p merging with %p\n", head, rightHead);
            #endif

            RemoveBlock(rightHead);

            head->SizeAndFlags += rightHead->SizeAndFlags;
            BlockEnd* rightEnd = head->GetEnd();
            rightEnd->Size = head->SizeAndFlags;
        }
    }

    AddBlock(head);
}

}

void* operator new(size_t size)
{
    return malloc(size);
}

void* operator new[](size_t size)
{
    return malloc(size);
}

void operator delete(void* addr)
{
    return free(addr);
}

void operator delete[](void* addr)
{
    return free(addr);
}

void* operator new(size_t, void* p)
{ 
    return p;
}

void* operator new[](size_t, void* p)
{
    return p;
}

void operator delete(void*, size_t)
{
}

void operator delete[](void*, size_t)
{
}
