/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <atomic>
#include <process.hpp>
#include <scopedlock.hpp>
#include <pmm.hpp>
#include <panic.hpp>
#include <compiler.hpp>

// TODO: verify this is our process in every vmm manipulation place

namespace tasking
{

static std::atomic<uint64_t> lastPid_ = { 0 };
static char kernelProcessName_[] = "kernel";

class KernelProcess : public Process
{
public:
    /**
     * Inits the kernel process
     * @param kernelEnd The kernel end address
     */
    KernelProcess(uintptr_t kernelEnd)
        // It's okay that this name is not freeable because we are never going to free it.
        : Process(kernelProcessName_)
    {
        mmapTree_.Add(0x00, kernelEnd);
    }
};

static KernelProcess* kernelProcess_;

Process::Process(char* imagePath)
    : imagePath_(imagePath), pid_(lastPid_++)
{
}

Process::~Process()
{
    delete[] imagePath_;
}

uintptr_t Process::AddMapping(uintptr_t addr, size_t size, mem::vmm::mapflags_t flags)
{
    sync::ScopedLock lock(mapLock_);

    uintptr_t virt = mmapTree_.FindGap(size);
    if(unlikely(!virt))
        return 0;

    if(unlikely(!mem::vmm::MapRange(virt, addr, size, flags)))
        return 0;

    if(unlikely(!mmapTree_.Add(virt, virt + size)))
    {
        mem::vmm::UnmapRange(virt, size);
        return 0;
    }

    return virt;
}

uintptr_t Process::AllocPhysicalAndMap(size_t size, mem::vmm::mapflags_t flags)
{
    uintptr_t virt;
    {
        sync::ScopedLock lock(mapLock_);

        virt = mmapTree_.FindGap(size);
        if(unlikely(!virt || !mmapTree_.Add(virt, virt + size)))
            return 0;
    }

    uintptr_t current = virt;
    size_t left = size;

    /**
     * Helper lambda to unmap and free everything we have already mapped and allocated.
     * Used in error handling.
     */
    auto FreeAndUnmap = [&]()
    {
        for(uintptr_t x = virt; x < current; x += mem::PAGE_SIZE)
        {
            mem::pmm::FreePage(mem::vmm::GetPhysicalAddress(x));
            mem::vmm::UnMap(x);
        }

        sync::ScopedLock lock(mapLock_);
        mmapTree_.Remove(virt, virt + size);
    };

    while(left)
    {
        uintptr_t phys = mem::pmm::AllocPage();
        if(unlikely(!phys))
        {
            FreeAndUnmap();
            return 0;
        }

        if(unlikely(!mem::vmm::Map(current, phys, flags)))
        {
            FreeAndUnmap();
            mem::pmm::FreePage(phys);
            return 0;
        }

        left -= mem::PAGE_SIZE;
        current += mem::PAGE_SIZE;
    }

    return virt;
}

bool Process::RemoveMapping(uintptr_t addr, size_t size)
{
    sync::ScopedLock lock(mapLock_);

    // TODO: implement this properly (so you can unmap parts of a mapping)
    // TODO: error checking
    mem::vmm::UnmapRange(addr, size);
    mmapTree_.Remove(addr, addr + size);

    return true;
}

void InitKernelProcess(uintptr_t kernelEnd)
{
    kernelProcess_ = new KernelProcess(kernelEnd);
}

Process* GetKernelProcess()
{
    return kernelProcess_;
}

}
