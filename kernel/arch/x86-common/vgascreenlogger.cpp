/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string.h>
#include <vmm.hpp>
#include <arch/x86-common/portio.hpp>
#include <arch/x86-common/vgascreenlogger.hpp>

namespace log
{

uint16_t* const   VIDMEM = (uint16_t*)0xB8000;
constexpr uint8_t ATTRIB = 0x07;

VGAScreenLogger::VGAScreenLogger()
{
    // Disable cursor
    arch::x86::portio::Out8(0x3D4, 0x0A);
    arch::x86::portio::Out8(0x3D5, 0x20);

    Clear();
}

void VGAScreenLogger::Clear() const
{
    for(int i = 0; i < width_ * height_; ++i)
        VIDMEM[i] = (ATTRIB << 8) | ' ';
}

void VGAScreenLogger::RenderChar(char c) const
{
    VIDMEM[cursorY_ * width_ + cursorX_] = (ATTRIB << 8) | c;
}

void VGAScreenLogger::Scroll() const
{
    memmove(VIDMEM, VIDMEM + width_, width_ * (height_ - 1) * 2);

    for(int i = 0; i < width_; ++i)
        VIDMEM[width_ * (height_ - 1) + i] = (ATTRIB << 8) | ' ';
}

}
