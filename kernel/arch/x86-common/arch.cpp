/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdint>
#include <string.h>
#include <arch/common/arch.hpp>
#include <arch/common/cpu.hpp>
#include <arch/x86-common/idt.hpp>
#include <arch/x86-common/lapic.hpp>
#include <arch/common/acpi/acpi.hpp>
#include <vmm.hpp>
#include <logger.hpp>
#include <panic.hpp>
#include <compiler.hpp>

#define LOG_PREFIX "ARCH"

namespace arch
{

namespace x86
{

static uint32_t ECXFeatures_, EDXFeatures_, ExtendedECXFeatures_;

void CPUID(uint32_t level, uint32_t* eax, uint32_t* ebx, uint32_t* ecx, uint32_t* edx)
{
    asm("cpuid" : "=a" (*eax), "=b" (*ebx), "=c" (*ecx), "=d" (*edx) : "a" (level));
}

void CPUID(uint32_t level, uint32_t subLeaf, uint32_t* eax, uint32_t* ebx, uint32_t* ecx, uint32_t* edx)
{
    asm("cpuid" : "=a" (*eax), "=b" (*ebx), "=c" (*ecx), "=d" (*edx) : "a" (level), "c" (subLeaf));
}

void ReadMSR(uint32_t reg, uint32_t* eax, uint32_t* edx)
{
    asm("rdmsr" : "=a" (*eax), "=d" (*edx) : "c" (reg));
}

void ReadMSR(MSR reg, uint32_t* eax, uint32_t* edx)
{
    ReadMSR(static_cast<uint32_t>(reg), eax, edx);
}

void WriteMSR(uint32_t reg, uint32_t eax, uint32_t edx)
{
    asm("wrmsr" : : "a" (eax), "d" (edx), "c" (reg));
}

void WriteMSR(MSR reg, uint32_t eax, uint32_t edx)
{
    WriteMSR(static_cast<uint32_t>(reg), eax, edx);
}

uint64_t ReadMSR(MSR reg)
{
    uint32_t eax, edx;
    ReadMSR(static_cast<uint32_t>(reg), &eax, &edx);
    return ((uint64_t)edx << 32) | eax;
}

bool BootCPUSupports(ECXFeature f)
{
    return ECXFeatures_ & static_cast<uint32_t>(f);
}

bool BootCPUSupports(ExtendedECXFeature f)
{
    return ExtendedECXFeatures_ & static_cast<uint32_t>(f);
}

bool BootCPUSupports(EDXFeature f)
{
    return EDXFeatures_ & static_cast<uint32_t>(f);
}

/**
 * Enable x86 specific features
 */
void EnableFeatures()
{
    size_t tmp;

    // UMIP
    // TODO: (see below)
    if(BootCPUSupports(ExtendedECXFeature::UMIP))
    {
        asm volatile("mov %%cr4, %0" : "=r" (tmp));
        tmp |= 1 << 11;
        asm volatile("mov %0, %%cr4" : : "r" (tmp));
    }

    // FPU & SSE
    asm volatile("mov %%cr0, %0" : "=r" (tmp));
    tmp &= ~(1 << 2);
    tmp |= (1 << 1);
    asm volatile("mov %0, %%cr0" : : "r" (tmp));
    asm volatile("mov %%cr4, %0" : "=r" (tmp));
    tmp |= (1 << 9) | (1 << 10);
    asm volatile("mov %0, %%cr4" : : "r" (tmp));
    asm volatile("fninit");

    // TODO: AVX? (the features need to be determined per CPU, same for UMIP)
}

/**
 * AP kernel entrypoint
 */
void APInit()
{
    cpu::InitAP();

    // TODO: every cpu same IDT?
    idt::Load();
    lapic::Init();
    EnableFeatures();
  asm volatile("sti");
    log::Logf("I'm an AP and my Id is %u\n", cpu::GetCurrentId());
    for(;;);
}

extern "C"
{
    extern uint8_t _binary_arch_x86_trampoline_ap_startup_bin_start[];
    extern uint8_t _binary_arch_x86_trampoline_ap_startup_bin_size[];
    extern uint8_t BootPDir[];
}

/**
 * Sets up the trampoline
 */
void SetupTrampoline()
{
    using namespace mem;

    // Setup trampoline
    vmm::Map(AP_TRAMPOLINE, AP_TRAMPOLINE, vmm::MapFlags::WRITABLE);
    uintptr_t start = (uintptr_t)_binary_arch_x86_trampoline_ap_startup_bin_start;
    uint32_t size = (uint32_t)_binary_arch_x86_trampoline_ap_startup_bin_size;
    memcpy((void*)AP_TRAMPOLINE, (void*)start, size);

    // Setup data required by trampoline
    *(cpuid_t*)AP_CPU_COUNT       = 1;
    *(uintptr_t*)AP_PAGEDIR       = (uintptr_t)BootPDir;
    *(uintptr_t*)AP_START_POINTER = (uintptr_t)&APInit;
}

}

void EarlyInit(uintptr_t rsdp)
{
    x86::idt::Prepare();
    x86::idt::Load();

    // Discover features
    uint32_t garbage, highest;
    x86::CPUID(0x00, &highest, &garbage, &garbage, &garbage);
    x86::CPUID(0x01, &garbage, &garbage, &x86::ECXFeatures_, &x86::EDXFeatures_);
    if(highest >= 0x07)
        x86::CPUID(0x07, 0x00, &garbage, &garbage, &x86::ExtendedECXFeatures_, &garbage);

    acpi::Init(reinterpret_cast<acpi::RSDPDescriptor*>(rsdp));//TODO: rename to earlyinit?
    mem::vmm::Init();
    x86::lapic::BSPSetup();
}

void LateInit()
{
    #ifndef NDEBUG
    char name[48];
    x86::CPUID(0x80000002, (uint32_t*)(name +  0), (uint32_t*)(name +  4), (uint32_t*)(name +  8), (uint32_t*)(name + 12));
    x86::CPUID(0x80000003, (uint32_t*)(name + 16), (uint32_t*)(name + 20), (uint32_t*)(name + 24), (uint32_t*)(name + 28));
    x86::CPUID(0x80000004, (uint32_t*)(name + 32), (uint32_t*)(name + 36), (uint32_t*)(name + 40), (uint32_t*)(name + 44));
    LOG("Boot CPU: %.48s\n", name);
    #endif

    x86::SetupTrampoline();
    x86::EnableFeatures();
}

void SpinHint()
{
    asm("pause");
}

void NoInt()
{
    asm("cli");
}

void Halt()
{
    asm("hlt");
}

cpuid_t GetAliveCPUCount()
{
    return *(volatile cpuid_t*)mem::AP_CPU_COUNT;
}

void BroadcastIPI(intvector_t vector)
{
    x86::lapic::BroadcastIPI(vector);
}

}
