/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <arch/common/arch.hpp>
#include <arch/common/cpu.hpp>
#include <arch/x86-common/portio.hpp>
#include <arch/x86-common/lapic.hpp>
#include <vmm.hpp>
#include <logger.hpp>
#include <panic.hpp>
#include <compiler.hpp>

#define LOG_PREFIX "LAPIC"

namespace arch::x86::lapic
{

/**
 * LAPIC registers
 */
enum class Reg : uint32_t
{
    ID             = 0x02,
    VERSION        = 0x03,
    SPURIOUS_IVR   = 0x0F,
    ICR            = 0x30,
    ICR_LO         = 0x30,
    ICR_HI         = 0x31,
    LVT_TIMER      = 0x32,
    TIMER_INIT_CNT = 0x38,
    TIMER_CURR_CNT = 0x39,
    TIMER_DCR      = 0x3E
};

typedef uint32_t intflags_t;

enum IntFlags : intflags_t
{
    // Delivery modes
    DELIVERY_FIXED   = 0b000 << 8,
    DELIVERY_SMI     = 0b010 << 8,
    DELIVERY_NMI     = 0b100 << 8,
    DELIVERY_INIT    = 0b101 << 8,
    DELIVERY_STARTUP = 0b110 << 8,

    // SIVR
    SIVR_ENABLE      = 1 << 8,

    // Timer modes
    TIMER_ONE_SHOT   = 0b00 << 18,
    TIMER_PERIODIC   = 0b00 << 18,

    // Destination mode
    DEST_PHYSICAL    = 0 << 11,
    DEST_LOGICAL     = 1 << 11,

    // Level
    LEVEL_DEASSERT   = 0 << 14,
    LEVEL_ASSERT     = 1 << 14,

    // Edge/Level triggered
    TRIGGER_EDGE     = 0 << 15,
    TRIGGER_LEVEL    = 1 << 15,

    // Mask
    MASKED           = 1 << 16,

    // Destination shorthand
    DEST_SELF        = 0b01 << 18,
    DEST_ALL_IN_SELF = 0b10 << 18,
    DEST_ALL_EX_SELF = 0b11 << 18
};

enum TimerDCR
{
    TIMER_DCR1   = 0b1011,
    TIMER_DCR2   = 0b0000,
    TIMER_DCR4   = 0b0001,
    TIMER_DCR8   = 0b0011,
    TIMER_DCR16  = 0b0011,
    TIMER_DCR32  = 0b1000,
    TIMER_DCR64  = 0b1001,
    TIMER_DCR128 = 0b1010
};

static uint64_t apicClock_;
static bool X2APIC_;
static uintptr_t lapicPhysAddress_;

/**
 * Remap the PIC and mask all the interrupts.
 * We do this because we're going to use the APIC instead.
 */
static void RemapAndMaskPIC()
{
    portio::Out8(0x20, 0x11);
    portio::Out8(0xA0, 0x11);
    portio::Out8(0x21, 0x20);
    portio::Out8(0xA1, 0x28);
    portio::Out8(0x21, 0x04);
    portio::Out8(0xA1, 0x02);
    portio::Out8(0x21, 0x01);
    portio::Out8(0xA1, 0x01);
    portio::Out8(0x21, 0xFF);
    portio::Out8(0xA1, 0xFF);
}

// TODO: document me
inline static volatile uint32_t* CalcLocation(Reg reg, uintptr_t base)
{
    return reinterpret_cast<volatile uint32_t*>(base | (static_cast<uint32_t>(reg) << 4));
}

// TODO: document me
inline static volatile uint32_t* CalcLocation(Reg reg)
{
    return CalcLocation(reg, mem::LAPIC_ADDRESS);
}

// TODO: document me
inline static uint32_t CalcReg(Reg reg)
{
    return 0x800 | static_cast<uint32_t>(reg);
}

// TODO: document me
static uint32_t Read32(Reg reg)
{
    if(X2APIC_)
    {
        uint32_t value, garbage;
        ReadMSR(CalcReg(reg), &value, &garbage);
        return value;
    }
    else
    {
        return *CalcLocation(reg);
    }
}

// TODO: document me
static void Write32(Reg reg, uint32_t value)
{
    if(X2APIC_)
        WriteMSR(CalcReg(reg), value, 0);
    else
        *CalcLocation(reg) = value;
}

/**
 * Calibrates the APIC timer
 */
static void CalibrateTimer()
{
    constexpr uint32_t PIT_FREQUENCY = 1193182;
    constexpr uint16_t WAIT_TIME_MS  = 8;
    constexpr uint16_t DIVISOR       = (uint16_t)(PIT_FREQUENCY / (1000 / WAIT_TIME_MS));
    constexpr auto     COUNT         = 2U;

    // Gate-2 clear
    x86::portio::Out8(0x61, x86::portio::In8(0x61) & ~0x01);
    // Init PIT: Channel 2, lo-hi byte access, mode 0
    x86::portio::Out8(0x43, 0x80 | 0x30 | 0x00);

    uint64_t ticks = 0;
    for(uint32_t i = 0; i < COUNT; ++i)
    {
        Write32(Reg::TIMER_INIT_CNT, 0xFFFFFFFF);

        // Setup time
        x86::portio::Out8(0x42, DIVISOR & 0xFF);
        x86::portio::Out8(0x42, DIVISOR >> 8);

        // Gate-2 raise
        x86::portio::Out8(0x61, x86::portio::In8(0x61) | 0x01);

        // Wait until counter hits zero
        while((x86::portio::In8(0x61) & 0x20) == 0);

        uint32_t count = 0xFFFFFFFF - Read32(Reg::TIMER_CURR_CNT);
        ticks += count;

        // Stop APIC timer
        Write32(Reg::TIMER_INIT_CNT, 0x00);
    }

    ticks /= COUNT;
    apicClock_ = ticks * (1000 / WAIT_TIME_MS);

    LOGV("Bus: %llu.%llu MHz clock\n", apicClock_ / 1000000, apicClock_ % 1000000);
}

/**
 * Switches mode if needed
 */
static void SwitchModeIfNeeded()
{
    if(X2APIC_)
    {
        uint32_t lo, hi;
        x86::ReadMSR(x86::MSR::APIC_BASE, &lo, &hi);
        x86::WriteMSR(x86::MSR::APIC_BASE, lo | (1 << 10), hi);
    }
}

void Prepare(uintptr_t phys)
{
    // Detect mode
    if(x86::BootCPUSupports(ECXFeature::X2APIC))
        X2APIC_ = true;

    SwitchModeIfNeeded();

    lapicPhysAddress_ = phys;

    if(X2APIC_)
        arch::SetBSPId(Read32(Reg::ID));
    else
        arch::SetBSPId(*CalcLocation(Reg::ID, phys) >> 24);
}

void BSPSetup()
{
    // TODO: aparte APSetup() maken waar ook init wordt in gecalled
    //          zodat we niet dubbel "SwitchModeIfNeeded" moeten aanroepen
    assert(lapicPhysAddress_ != 0);

    RemapAndMaskPIC();

    if(!X2APIC_)
    {
        /**
         * The MMIO mapping needs to be done only once because it is reused for each CPU.
         * The page is reserved, so mapping cannot fail.
         */
        using namespace mem::vmm;
        Map(mem::LAPIC_ADDRESS, lapicPhysAddress_, MapFlags::WRITABLE
                                                    | MapFlags::GLOBAL
                                                    | MapFlags::NO_EXEC
                                                    | MapFlags::CACHE_UNCACHED);
    }

    Init();
    CalibrateTimer();
}

void Init() // TODO: rename naar CommonInit en maak "static"
{
    SwitchModeIfNeeded();
    Write32(Reg::LVT_TIMER, IntFlags::TIMER_ONE_SHOT | IntFlags::MASKED);
    Write32(Reg::SPURIOUS_IVR, IntFlags::SIVR_ENABLE | VECTOR_SPURIOUS);
    Write32(Reg::TIMER_DCR, TIMER_DCR1);
}

static void SendRawIPI(cpuid_t cpuid, uint32_t flags)
{
    if(X2APIC_)
    {
        /**
         * According to "Intel(R) 64 Architecture x2APIC Specification",
         * we don't need to read the status in x2apic mode.
         */
        WriteMSR(CalcReg(Reg::ICR), flags, cpuid);
    }
    else
    {
        /**
         * Bit 12 is the delivery status.
         * We always use this when sending IPIs, so we are guaranteed there
         * is no pending IPI when we enter this function.
         */
        Write32(Reg::ICR_HI, cpuid << (56 - 32));
        Write32(Reg::ICR_LO, flags);
        while(Read32(Reg::ICR_LO) & (1 << 12));
    }
}

void SendIPI(cpuid_t cpuid, intvector_t vector)
{
    SendRawIPI(cpuid, IntFlags::DELIVERY_FIXED
                        | IntFlags::DEST_PHYSICAL 
                        | IntFlags::LEVEL_ASSERT
                        | IntFlags::TRIGGER_EDGE
                        | vector);
}

void BroadcastIPI(intvector_t vector)
{
    SendRawIPI(0, IntFlags::DELIVERY_FIXED
                    | IntFlags::DEST_ALL_EX_SELF 
                    | IntFlags::LEVEL_ASSERT
                    | IntFlags::TRIGGER_EDGE
                    | vector);
}

void StartCPUs(cpu::CPU* first, cpu::CPU* last)
{
    assert(first < last);

    using namespace mem;

    constexpr uint32_t SEGMENT = (AP_TRAMPOLINE >> 12) & 0xFF;

    constexpr auto INIT_FLAGS = IntFlags::DELIVERY_INIT
                                | IntFlags::DEST_PHYSICAL 
                                | IntFlags::LEVEL_ASSERT
                                | IntFlags::TRIGGER_EDGE;

    constexpr auto SIPI_FLAGS = IntFlags::DELIVERY_STARTUP
                                | IntFlags::DEST_PHYSICAL 
                                | IntFlags::LEVEL_ASSERT
                                | IntFlags::TRIGGER_EDGE
                                | SEGMENT;

    // Setup data required by trampoline
    *(uintptr_t*)AP_CPU_DATA = AP_FIRST_DATA;

    // Send INIT IPI and init trampoline-specific data
    int i = 0;
    for(auto cpu = first; cpu < last; ++cpu, ++i)
    {
        SendRawIPI(cpu->Id, INIT_FLAGS);
        // TODO
        assert(false);
/*
        constexpr auto STACK_SIZE = 16384U;
        uintptr_t stack = tasking::GetKernelProcess()->AllocPhysicalAndMap(STACK_SIZE, vmm::MapFlags::WRITABLE
                                                                                        | vmm::MapFlags::NO_EXEC
                                                                                        | vmm::MapFlags::GLOBAL);

        if(unlikely(!stack))
            runtime::Panic("Could not allocate stack for CPU");

        *((uintptr_t*)AP_FIRST_DATA + (2 * i + 0)) = stack + STACK_SIZE;
        *((uintptr_t*)AP_FIRST_DATA + (2 * i + 1)) = (uintptr_t)cpu;*/
    }

    TimerWait(10000);

    cpuid_t aliveTarget = arch::GetAliveCPUCount() + (last - first);

    // Send SIPI
    for(auto cpu = first; cpu < last; ++cpu)
        SendRawIPI(cpu->Id, SIPI_FLAGS);

    /**
     * Waits until CPUs start up with a timeout
     * @param timeout The timeout in us
     * @return True if not all started, false otherwise
     */
    auto WaitStartup = [aliveTarget](uint32_t timeout)
    {
        constexpr uint32_t SINGLE_WAIT_TIME = 40;

        bool notStarted;

        uint32_t tries = 0;
        while(tries++ < timeout / SINGLE_WAIT_TIME && (notStarted = (arch::GetAliveCPUCount() < aliveTarget)))
            TimerWait(SINGLE_WAIT_TIME);

        return notStarted;
    };

    // Not all CPUs started in time?
    if(WaitStartup(200))
    {
        // We can figure out which ones did not start using the pointer array
        for(auto cpu = first; cpu < last; ++cpu)
        {
            if(!cpu->Alive)
            {
                #if 0
                LOG("CPU %u did not start, retrying\n", cpu->Id);
                #endif
                SendRawIPI(cpu->Id, SIPI_FLAGS);
            }
        }

        if(WaitStartup(200 * 1000))
        {
            // Still didn't start all...
            for(auto cpu = first; cpu < last; ++cpu)
            {
                if(!cpu->Alive)
                    LOG("CPU %u did not start, giving up\n", cpu->Id);
            }
        }
    }
}

void TimerWait(uint64_t us)
{
    assert(us != 0);
    assert(1000000 / us != 0);

    uint64_t count = apicClock_ / (1000000 / us);
    assert(count < 0xFFFFFFFF);

    // We never change the timer mode to anything else than one-shot
    Write32(Reg::TIMER_INIT_CNT, (uint32_t)count);
    while(Read32(Reg::TIMER_CURR_CNT) != 0);
}

cpuid_t GetId()
{
    if(X2APIC_)
        return Read32(Reg::ID);
    else
        return Read32(Reg::ID) >> 24;
}

}
