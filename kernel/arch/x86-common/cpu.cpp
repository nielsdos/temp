/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <arch/common/arch.hpp>
#include <arch/common/cpu.hpp>
#include <heap.hpp>
#include <compiler.hpp>
#include <panic.hpp>

namespace arch::cpu
{

static CPU* cpus_;
static cpuid_t cpuCount_;

// TODO
static CPU tmpBSPData_;

void Lel()
{
    tmpBSPData_.Alive = true;
    tmpBSPData_.Id = arch::GetBSPId();
    InitBSP(&tmpBSPData_);
}

void InitBSP(cpu::CPU* cpu)
{
    uintptr_t addr = reinterpret_cast<uintptr_t>(cpu);

    #if defined(__i386__)
    x86::WriteMSR(x86::MSR::FS_BASE, addr, 0);
    #else /* __x86_64__ */
    #error "Unimplemented"
    #endif
}

void SetCPUs(cpu::CPU* cpus, cpuid_t cpuCount)
{
    assert(cpus_ == nullptr);
    cpus_ = cpus;
    cpuCount_ = cpuCount;
}

Tuple<cpu::CPU*, cpu::CPU*> GetCPUs()
{
    assert(cpus_ != nullptr);
    return Tuple(cpus_, cpus_ + cpuCount_);
}

void InitAP()
{
    // TODO: ditch this and others
    CPU_SET(Alive, true);
}

cpuid_t GetCurrentId()
{
    return CPU_GET(Id);
}

bool IsCPUDataInitialized()
{
    uint32_t eax, edx;
    #if defined(__i386__)
    x86::ReadMSR(x86::MSR::FS_BASE, &eax, &edx);
    return eax;
    #else /* __x86_64__ */
    #error "Unimplemented"
    #endif
}

}
