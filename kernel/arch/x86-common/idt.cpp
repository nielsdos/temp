/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <arch/common/arch.hpp>
#include <arch/x86-common/idt.hpp>
#include <logger.hpp>
#include <panic.hpp>

namespace arch::x86::idt
{

enum Flags : uint8_t
{
    PRESENT  = 0x80,
    INT_GATE = 0x0E,
    TRAP     = 0x0F,
    RING0    = 0x00,
    RING3    = 0x60
};

constexpr uint16_t KERNEL_SEL = 0x08;
constexpr uint16_t USER_SEL   = 0x1B;

struct IDTEntry
{
    uint16_t BaseLow;
    uint16_t Selector;
    uint8_t  Zero;
    uint8_t  Flags;
    uint16_t BaseHigh;
} __attribute__((packed));

IDTEntry IDTEntries[256];

extern "C"
{
    extern void ISR0();
    extern void ISR1();
    extern void ISR2();
    extern void ISR3();
    extern void ISR4();
    extern void ISR5();
    extern void ISR6();
    extern void ISR7();
    extern void ISR8();
    extern void ISR9();
    extern void ISR10();
    extern void ISR11();
    extern void ISR12();
    extern void ISR13();
    extern void ISR14();
    extern void ISR15();
    extern void ISR16();
    extern void ISR17();
    extern void ISR18();
    extern void ISR19();
    extern void ISR20();
    extern void ISR21();
    extern void ISR22();
    extern void ISR23();
    extern void ISR24();
    extern void ISR25();
    extern void ISR26();
    extern void ISR27();
    extern void ISR28();
    extern void ISR29();
    extern void ISR30();
    extern void ISR31();
    extern void IPIHalt();
    extern void IntSpurious();
}

/**
 * Sets an idt entry
 * @param intNum The interrupt number
 * @param base The base address
 * @param selector The code selector
 * @param flags The flags
 */
void SetEntry(uint8_t intNum, uint32_t base, uint16_t selector, uint8_t flags)
{
    IDTEntries[intNum].BaseLow  = base & 0xFFFF;
    IDTEntries[intNum].BaseHigh = (base >> 16) & 0xFFFF;
    IDTEntries[intNum].Selector = selector;
    IDTEntries[intNum].Zero     = 0;
    IDTEntries[intNum].Flags    = flags;
}

void Prepare()
{
    // Exception handlers
    SetEntry(0,  (uint32_t)ISR0,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(1,  (uint32_t)ISR1,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(2,  (uint32_t)ISR2,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(3,  (uint32_t)ISR3,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(4,  (uint32_t)ISR4,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(5,  (uint32_t)ISR5,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(6,  (uint32_t)ISR6,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(7,  (uint32_t)ISR7,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(8,  (uint32_t)ISR8,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(9,  (uint32_t)ISR9,  KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(10, (uint32_t)ISR10, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(11, (uint32_t)ISR11, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(12, (uint32_t)ISR12, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(13, (uint32_t)ISR13, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(14, (uint32_t)ISR14, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(15, (uint32_t)ISR15, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(16, (uint32_t)ISR16, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(17, (uint32_t)ISR17, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(18, (uint32_t)ISR18, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(19, (uint32_t)ISR19, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(20, (uint32_t)ISR20, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(21, (uint32_t)ISR21, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(22, (uint32_t)ISR22, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(23, (uint32_t)ISR23, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(24, (uint32_t)ISR24, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(25, (uint32_t)ISR25, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(26, (uint32_t)ISR26, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(27, (uint32_t)ISR27, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(28, (uint32_t)ISR28, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(29, (uint32_t)ISR29, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(30, (uint32_t)ISR30, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(31, (uint32_t)ISR31, KERNEL_SEL, PRESENT | INT_GATE | RING0);

    // TODO: temp hack
    for(int i = 32; i < 253; ++i)
        SetEntry(i, (uint32_t)ISR0, KERNEL_SEL, PRESENT | INT_GATE | RING0);

    SetEntry(VECTOR_SPURIOUS, (uint32_t)IntSpurious, KERNEL_SEL, PRESENT | INT_GATE | RING0);
    SetEntry(VECTOR_IPI_HALT, (uint32_t)IPIHalt, KERNEL_SEL, PRESENT | INT_GATE | RING0);
}

void Load()
{
    // Load IDT
    uint16_t limit = sizeof(IDTEntries) - 1;
    uint32_t base  = (uint32_t)&IDTEntries;

    #ifdef __i386__
    asm volatile("subl $6, %%esp;"
                 "movw %w0, 0(%%esp);"
                 "movl %1, 2(%%esp);"
                 "lidt (%%esp);"
                 "addl $6, %%esp" : : "rN" (limit), "r" (base));
    #else
    #error "Unimplemented"
    #endif
}

struct Regs
{
    uint32_t es, ds;
    uint32_t eax, ecx, edx, ebx, ebp, esi, edi;
    uint32_t IntNr, ErrorCode;
    uint32_t eip, cs, eflags, esp, ss;
};

const char* Exceptions_[] =
{
    "Divide by zero",
    "Debug",
    "NMI",
    "Breakpoint",
    "Overflow",
    "Bound range exceeded",
    "Invalid opcode",
    "Device not available",
    "Double fault",
    "No coprocessor",
    "Invalid TSS",
    "Segment not present",
    "Stack segment fault",
    "General protection fault",
    "Page fault",
    "?",
    "FPU exception",
    "Alignment check",
    "Machine check",
    "SIMD FP exception",
    "Virtualization exception",
    "?", "?", "?", "?", "?", "?", "?", "?", "?",
    "Security exception",
    "?"
};

extern "C"
void IntHandler(Regs* ctx)
{
    // ISR exception
    if(ctx->IntNr < 32)
    {
        uint32_t cr2, cr3;
        asm("movl %%cr2, %0" : "=r" (cr2));
        asm("movl %%cr3, %0" : "=r" (cr3));

        runtime::Panic("(ISR) %s\tErrorcode: %u\n"
                       "EAX %.8x\tEBX %.8x\tECX %.8x\tEDX %.8x\n"
                       "ESP %.8x\tEBP %.8x\tESI %.8x\tEDI %.8x\n"
                       "CR2 %.8x\tCR3 %.8x\tEIP %.8x\n"
                       "CS %.4x\t\tDS %.4x\t\tES %.4x\n",
                       Exceptions_[ctx->IntNr], ctx->ErrorCode,
                       ctx->eax, ctx->ebx, ctx->ecx, ctx->edx,
                       ctx->esp, ctx->ebp, ctx->esi, ctx->edi,
                       cr2, cr3, ctx->eip,
                       ctx->cs & 0xFFFF, ctx->ds & 0xFFFF, ctx->es & 0xFFFF);
    }

    log::Logf("int=%u\n", ctx->IntNr);
}

}
