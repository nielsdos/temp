/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <arch/x86-common/ioapic.hpp>
#include <process.hpp>
#include <compiler.hpp>
#include <panic.hpp>

namespace arch::x86::ioapic
{

enum class IOAPIC::Reg : uint32_t
{
    ID = 0x00,
    VERSION = 0x01
};

IOAPIC::IOAPIC(uint32_t GSIBase, uintptr_t addr)
    : GSIBase_(GSIBase)
{
    using namespace mem;
    uintptr_t v = tasking::GetKernelProcess()->AddMapping(addr, PAGE_SIZE, vmm::MapFlags::WRITABLE
                                                                            | vmm::MapFlags::NO_EXEC
                                                                            | vmm::MapFlags::CACHE_UNCACHED);

    if(unlikely(!v))
        runtime::Panic("[IOAPIC] Cannot map I/O Apic");
    
    virt_ = addr - vmm::AlignDown(addr) + v;

    redirEntries_ = 1 + ((Read(Reg::VERSION) >> 16) & 0xFF);
}

bool IOAPIC::IsResponsibleFor(uint32_t gsi) const
{
    return gsi >= GSIBase_ && gsi < GSIBase_ + redirEntries_;
}

uint32_t IOAPIC::Read(Reg reg) const
{
    *(volatile uint32_t*)virt_ = static_cast<uint32_t>(reg);
    return *(volatile uint32_t*)(virt_ | 0x10);
}

void IOAPIC::Write(Reg reg, uint32_t value) const
{
    *(volatile uint32_t*)virt_ = static_cast<uint32_t>(reg);
    *(volatile uint32_t*)(virt_ | 0x10) = value;
}

IOAPIC* IOAPICList_ = nullptr;

void Register(IOAPIC* ioa)
{
    if(unlikely(!IOAPICList_))
        IOAPICList_ = ioa;
    else
    {
        IOAPIC* last = IOAPICList_;
        while(last->Next)
            last = last->Next;
        
        last->Next = ioa;
    }
}

IOAPIC* GetFor(uint32_t gsi)
{
    IOAPIC* current = IOAPICList_;
    while(current)
    {
        if(current->IsResponsibleFor(gsi))
            return current;

        current = current->Next;
    }

    return nullptr;
}

}
