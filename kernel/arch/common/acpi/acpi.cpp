/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <arch/common/acpi/acpi.hpp>
#include <arch/common/acpi/madt.hpp>
#include <arch/common/acpi/srat.hpp>
#include <arch/common/acpi/slit.hpp>
#include <logger.hpp>
#include <panic.hpp>
#include <process.hpp>
#include <compiler.hpp>

#define LOG_PREFIX "ACPI"

namespace arch::acpi
{

constexpr size_t TWO_PAGES = 2 * mem::PAGE_SIZE;

constexpr uint32_t SIG_MADT = 0x43495041;
constexpr uint32_t SIG_SRAT = 0x54415253;
constexpr uint32_t SIG_SLIT = 0x54494C53;

struct RSDPDescriptor
{
    char     Signature[8];
    uint8_t  Checksum;
    char     OEMID[6];
    uint8_t  Revision;
    uint32_t RSDTAddress;

    // Since version 2
    uint32_t Length;
    uint64_t XSDTAddress;
    uint8_t  ExtendedChecksum;
    uint8_t  Reserved[3];
} __attribute__((packed));

struct RSDT
{
    SDTHeader Header;
    uint32_t  Entries[];
} __attribute__((packed));

struct XSDT
{
    SDTHeader Header;
    uint64_t  Entries[];
} __attribute__((packed));

bool IsValidSDT(const SDTHeader* header)
{
    uint8_t sum = 0;
    const uint8_t* bytes = reinterpret_cast<const uint8_t*>(header);
    for(uint32_t i = 0; i < header->Length; ++i)
        sum += bytes[i];
    
    return (sum == 0);
}

const SDTHeader* MapSDT(const SDTHeader* header)
{
    /**
     * Map at least two pages so we can read the length.
     * If the length is bigger than two pages, we can increase the size of the mapping.
     */
    uintptr_t addr = reinterpret_cast<uintptr_t>(header);
    uintptr_t alignedDown = mem::vmm::AlignDown(addr);
    uintptr_t offset = addr - alignedDown;
    uintptr_t virt = tasking::GetKernelProcess()->AddMapping(alignedDown, TWO_PAGES, mem::vmm::MapFlags::NO_EXEC);

    if(unlikely(!virt))
        return nullptr;

    header = reinterpret_cast<const SDTHeader*>(virt + offset);

    size_t size = mem::vmm::AlignUp(addr + header->Length) - alignedDown;

    // Remove old mapping and map bigger area
    if(size > TWO_PAGES)
    {
        tasking::GetKernelProcess()->RemoveMapping(alignedDown, TWO_PAGES);

        virt = tasking::GetKernelProcess()->AddMapping(alignedDown, size, mem::vmm::MapFlags::NO_EXEC);
        if(unlikely(!virt))
            return nullptr;

        header = reinterpret_cast<const SDTHeader*>(virt + offset);
    }

    return header;
}

bool UnmapSDT(const SDTHeader* header)
{
    uintptr_t addr = reinterpret_cast<uintptr_t>(header);
    uintptr_t alignedDown = mem::vmm::AlignDown(addr);

    size_t size = mem::vmm::AlignUp(addr + header->Length) - alignedDown;
    if(size < TWO_PAGES)
        size = TWO_PAGES;

    return tasking::GetKernelProcess()->RemoveMapping(alignedDown, size);
}

void Init(const RSDPDescriptor* rsdp)
{
    /**
     * We have got a copy of the RSDP.
     * The bootloader also checks the RSDP checksum, we don't have to do it anymore.
     */
    if(unlikely(!rsdp))
        runtime::Panic("[ACPI] RSDP invalid or not found");

    bool rootIsRSDT = true;

    // Version 2 or higher should use XSDT instead of RSDT
    if(rsdp->Revision > 0 && rsdp->XSDTAddress)
    {
        #if defined(__i386__)
        if(rsdp->XSDTAddress < 0xFFFFFFFF)
            rootIsRSDT = false;
        #else
        rootIsRSDT = false;
        #endif
    }

    auto rootTable = reinterpret_cast<const SDTHeader*>(
                            rootIsRSDT ? rsdp->RSDTAddress : (uintptr_t)rsdp->XSDTAddress);
    
    if(unlikely(!IsValidSDT(rootTable)))
        runtime::Panic("[ACPI] Invalid or unreadable root");

    uint32_t entrySize  = rootIsRSDT ? 4 : 8;
    uint32_t entryCount = (rootTable->Length - sizeof(SDTHeader)) / entrySize;

    #if defined(__i386__) || defined(__x86_64__)
    const madt::MADT* madt = nullptr;
    #endif
    const srat::SRAT* srat = nullptr;
    const slit::SLIT* slit = nullptr;

    for(uint32_t i = 0; i < entryCount; ++i)
    {
        auto ptr = (rootIsRSDT) ?
                reinterpret_cast<const RSDT*>(rootTable)->Entries[i] : reinterpret_cast<const XSDT*>(rootTable)->Entries[i];

        auto header = reinterpret_cast<const SDTHeader*>(ptr);
        if(!IsValidSDT(header))
            continue;

        /**
         * x86 specific stuff.
         */
        #if defined(__i386__) || defined(__x86_64__)
        if(header->Signature.Num == SIG_MADT)
            madt = reinterpret_cast<const madt::MADT*>(header);
        else
        #endif

        if(header->Signature.Num == SIG_SRAT)
            srat = reinterpret_cast<const srat::SRAT*>(header);
        else if(header->Signature.Num == SIG_SLIT)
            slit = reinterpret_cast<const slit::SLIT*>(header);
    }

    // TODO: make sure this message is visible
    #if defined(__i386__) || defined(__x86_64__)
    /**
     * We don't support the old, obsolete PIC. We require an APIC.
     * Check and notify user.
     */
    if(unlikely(!madt))
        runtime::Panic("[ACPI] No APIC table found. You need to support and enable the APIC.");

    madt::EarlyHandle(madt);

    // TODO: this whole method should return a status I guess

    // TODO
    //madt::Handle(madt);
    #endif

    if(srat)
        srat::BuildMemoryDomains(srat);
}

}
