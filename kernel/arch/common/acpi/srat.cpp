/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdint>
#include <arch/common/arch.hpp>
#include <arch/common/cpu.hpp>
#include <arch/common/acpi/acpi.hpp>
#include <arch/common/acpi/srat.hpp>
#include <numa.hpp>
#include <pmm.hpp>
#include <logger.hpp>
#include <panic.hpp>

#define LOG_PREFIX "ACPI:SRAT"

namespace arch::acpi::srat
{

struct SRAT
{
    SDTHeader Header;
    uint32_t  Reserved1;
    uint64_t  Reserved2;
} __attribute__((packed));

enum class EntryType : uint8_t
{
    CPUAffinity = 0,
    MemoryAffinity = 1,
    CPUX2Affinity = 2
};

struct EntryHeader
{
    EntryType Type;
    uint8_t   Length;
} __attribute__((packed));

struct CPUAffinityEntry
{
    EntryHeader Header;
    uint8_t     DomainLo;       // Bits 0-7 of proximity domain
    uint8_t     Id;
    uint32_t    Flags;          // Bit 0: if zero => ignore
    uint8_t     SAPICEID;
    uint8_t     DomainHi[3];    // Bits 8-31 of proximity domain
    uint32_t    ClockDomain;
} __attribute__((packed));

struct CPUX2AffinityEntry
{
    EntryHeader Header;
    uint16_t    Reserved1;
    uint32_t    Domain;
    uint32_t    Id;
    uint32_t    Flags;          // Bit 0: if zero => ignore
    uint32_t    ClockDomain;
    uint32_t    Reserved2;
} __attribute__((packed));

struct MemoryAffinityEntry
{
    EntryHeader Header;
    uint32_t    Domain;
    uint16_t    Reserved1;
    uint64_t    Base;
    uint64_t    Length;
    uint32_t    Reserved2;
    /**
     * If flags bit 0 is zero => ignore
     * Bit 1: Hot pluggable
     * Bit 2: Non volatile
     */
    uint32_t    Flags;
    uint64_t    Reserved3;
} __attribute__((packed));

// See ACPI documentation
static_assert(sizeof(CPUAffinityEntry) == 16);
static_assert(sizeof(CPUX2AffinityEntry) == 24);
static_assert(sizeof(MemoryAffinityEntry) == 40);

void BuildMemoryDomains(const SRAT* table)
{
    ACPITable<SRAT, EntryHeader> srat(table);

    mem::EnableDomains();
    auto memDomains = mem::GetMemDomains();

    // TODO: is de return correct? meot er geen false teruggegeven wordne oid?
                // wss disable domains?

    for(auto const& header : srat)
    {
        switch(header.Type)
        {
            case EntryType::CPUAffinity:
            {
                auto entry = reinterpret_cast<const CPUAffinityEntry&>(header);
                if(likely(entry.Flags & 0x01) && entry.Id == arch::GetBSPId())
                {
                    arch::SetBSPDomain(entry.DomainLo
                                        | entry.DomainHi[0] << 8
                                        | entry.DomainHi[1] << 16
                                        | entry.DomainHi[2] << 24);
                }

                break;
            }

            case EntryType::CPUX2Affinity:
            {
                auto entry = reinterpret_cast<const CPUX2AffinityEntry&>(header);
                if(likely(entry.Flags & 0x01) && entry.Id == arch::GetBSPId())
                    arch::SetBSPDomain(entry.Domain);

                break;
            }

            case EntryType::MemoryAffinity:
            {
                auto entry = reinterpret_cast<const MemoryAffinityEntry&>(header);

                LOGV("Memory affinity, Domain: %u Base: 0x%llx Length: 0x%llx, %s\n",
                        entry.Domain, entry.Base, entry.Length, (entry.Flags & 0x01) ? "enabled" : "disabled");

                if(likely(entry.Flags & 0x01))
                {
                    auto domain = memDomains->GetDomain(entry.Domain);
                    if(!domain)
                    {
                        LOGV("Cannot place domain %u, skipping NUMA\n", entry.Domain);
                        return;
                    }

                    if(unlikely(!domain->Mem.EmplaceSorted((uint64_t)entry.Base, (uint64_t)entry.Length)))
                    {
                        LOGV("Cannot place memory %llx - %llx in domain %u, skipping NUMA\n",
                                entry.Base, entry.Length, entry.Domain);
                        return;
                    }
                }

                break;
            }
        }
    }
}

void Handle(const SRAT* table)
{
    ACPITable<SRAT, EntryHeader> srat(table);
    //auto [first, last] = cpu::GetCPUs();

    //mem::NUMA numa;

    // TODO: SLIT
    // TODO: plaats kernel stack en per-cpu data struct(?) in eigen memory unit
    // TODO: na splitsen opkuisen, wss gewoon met if?

    /**
     * Helper lambda to process a CPU
     * @param id The CPU Id
     * @param domain The domain Id
     * @param flags The flags, see the LAPIC structs
     */
    /*auto ProcessCPU = [&](uint32_t id, uint32_t domain, uint32_t flags)
    {
        if(likely(flags & 0x01))
        {
            for(cpu::CPU* cpu = first; cpu < last; ++cpu)
            {
                if(cpu->Id == id)
                {
                    cpu->Domain = domain;
                    return;
                }
            }

            runtime::Panic("[SRAT] CPU %u is not found", id);
        }
    };*/
/*
    for(auto const& header : srat)
    {
        switch(header.Type)
        {
            case EntryType::CPUAffinity:
            {
                auto entry = reinterpret_cast<const CPUAffinityEntry&>(header);
                uint32_t domain = entry.DomainLo
                                    | entry.DomainHi[0] << 8
                                    | entry.DomainHi[1] << 16
                                    | entry.DomainHi[2] << 24;

                LOGV("CPU affinity, Id: %u Domain: %u, %s\n", entry.Id, domain,
                    (entry.Flags & 0x01) ? "enabled" : "disabled");

                //ProcessCPU(entry.Id, domain, entry.Flags);

                break;
            }

            case EntryType::CPUX2Affinity:
            {
                auto entry = reinterpret_cast<const CPUX2AffinityEntry&>(header);

                LOGV("CPU X2 affinity, Id: %u Domain: %u, %s\n", entry.Id, entry.Domain,
                    (entry.Flags & 0x01) ? "enabled" : "disabled");

                //ProcessCPU(entry.Id, entry.Domain, entry.Flags);

                break;
            }

            case EntryType::MemoryAffinity:
            {
                auto entry = reinterpret_cast<const MemoryAffinityEntry&>(header);

                LOGV("Memory affinity, Domain: %u Base: 0x%llx Length: 0x%llx, %s\n",
                        entry.Domain, entry.Base, entry.Length, (entry.Flags & 0x01) ? "enabled" : "disabled");

                if(likely(entry.Flags & 0x01))
                {
                    auto domain = numa.GetDomain(entry.Domain);
                    if(!domain)
                    {
                        LOG("Cannot place domain %u, skipping NUMA\n", entry.Domain);
                        return;
                    }

                    if(unlikely(!domain->Mem.EmplaceSorted((uint64_t)entry.Base, (uint64_t)entry.Length)))
                    {
                        LOG("Cannot place memory %llx - %llx in domain %u, skipping NUMA\n",
                                entry.Base, entry.Length, entry.Domain);
                        return;
                    }
                }

                break;
            }

            default:
                LOGV("entry %hhu\n", static_cast<uint8_t>(header.Type));
                break;
        }
    }

    log::Logf("%d\n", sizeof(numa));

    numa.PrintDomains();*/
}

}
