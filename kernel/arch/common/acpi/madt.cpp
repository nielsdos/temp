/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdint>
#include <arch/common/arch.hpp>
#include <arch/common/acpi/acpi.hpp>
#include <arch/common/acpi/madt.hpp>
#include <arch/x86-common/ioapic.hpp>
#include <arch/x86-common/lapic.hpp>
#include <arch/common/cpu.hpp>
#include <heap.hpp>
#include <logger.hpp>
#include <panic.hpp>
#include <compiler.hpp>

// TODO
#include <assert.h>

#define LOG_PREFIX "ACPI:MADT"

namespace arch::acpi::madt
{

struct MADT
{
    SDTHeader Header;
    uint32_t  LAPICAddress;
    uint32_t  Flags;
} __attribute__((packed));

enum class EntryType : uint8_t
{
    LAPIC = 0,
    IOAPIC = 1,
    INT_SRC_OVERRIDE = 2,
    LAPIC_ADDR_OVERRIDE = 5,
    LAPICX2 = 9
};

struct EntryHeader
{
    EntryType Type;
    uint8_t   Length;
} __attribute__((packed));

struct LAPICEntry
{
    EntryHeader Header;
    uint8_t     ACPIUId;
    uint8_t     Id;
    uint32_t    Flags;      // Bit 0: if zero => CPU ignore
} __attribute__((packed));

// Basically the X2APIC version of the regular entry
struct LAPICX2Entry
{
    EntryHeader Header;
    uint16_t    Reserved;
    uint32_t    Id;
    uint32_t    Flags;      // See LAPICEntry
    uint32_t    ACPIUId;
} __attribute__((packed));

struct IOAPICEntry
{
    EntryHeader Header;
    uint8_t     Id;
    uint8_t     Reserved;
    uint32_t    Address;
    uint32_t    GSIBase;
} __attribute__((packed));

struct IntSrcOverrideEntry
{
    EntryHeader Header;
    uint8_t     Bus;
    uint8_t     Source;
    uint32_t    GSI;
    struct
    {
        /**
         * 00 -> conforms to bus specification
         * 01 -> active high
         * 11 -> active low
         */
        uint16_t Polarity : 2;
        /**
         * 00 -> conforms to bus specification
         * 01 -> edge-triggered
         * 11 -> level-triggered
         */
        uint16_t Trigger : 2;
        uint16_t Reserved : 12;
    }           Flags;
} __attribute__((packed));

struct LAPICAddrOverrideEntry
{
    EntryHeader Header;
    uint16_t    Reserved;
    uint64_t    LAPICAddress;
} __attribute__((packed));

// See ACPI documentation
static_assert(sizeof(LAPICEntry) == 8);
static_assert(sizeof(LAPICX2Entry) == 16);
static_assert(sizeof(IOAPICEntry) == 12);
static_assert(sizeof(IntSrcOverrideEntry) == 10);
static_assert(sizeof(LAPICAddrOverrideEntry) == 12);

void EarlyHandle(const MADT* table)
{
    x86::lapic::Prepare(table->LAPICAddress);
}

void Handle(const MADT* table)
{
    assert(false);
    ACPITable<MADT, EntryHeader> madt(table);

    cpuid_t myId = x86::lapic::GetId();// TODO: hoeft niet via lapic
    // TODO: wss kan lapic::GetId weg...
    cpuid_t cpuCount = 0;

    /**
     * We need to count the amount of LAPIC (X2) entries first
     * so we can allocate the required amount of per-cpu structures before doing any processing.
     */
    for(auto const& header : madt)
    {
        if(header.Type == EntryType::LAPIC)
        {
            if(likely(reinterpret_cast<const LAPICEntry&>(header).Flags & 0x01))
                ++cpuCount;
        }
        else if(header.Type == EntryType::LAPICX2)
        {
            if(likely(reinterpret_cast<const LAPICX2Entry&>(header).Flags & 0x01))
                ++cpuCount;
        }
    }

    // TODO: per domain cpu struct allocaten op een of andere manier
    // TODO: in UMA systemen een "fake" domein opstellen dat alles overspanned in early acpi ofzo?
    // TODO: per domain een variant van de pagetables houden wss (doen in vmm init oid)
    cpu::CPU* cpus = (cpu::CPU*)aligned_alloc(cpu::FALSE_SHARING_THRESHOLD, sizeof(cpu::CPU) * cpuCount);
    if(unlikely(!cpus))
        runtime::Panic("Could not allocate CPU structures");

    // First entry for BSP
    cpuid_t startupIndex = 0;
    cpus[0].Id = myId;
    cpus[0].Alive = true;
    cpus[0].Domain = 0;//TODO
    cpu::InitBSP(&cpus[0]);
    ++cpus;

    /**
     * Helper lambda to process a CPU
     * @param id The CPU Id
     * @param flags The flags, see the LAPIC structs
     */
    auto ProcessCPU = [&](uint32_t id, uint32_t flags)
    {
        if(likely((flags & 0x01) && id != myId))
        {
            cpus[startupIndex].Id = id;
            cpus[startupIndex].Alive = false;
            cpus[startupIndex].Domain = 0;//TODO
            ++startupIndex;

            if(startupIndex % x86::lapic::CPU_MAX_START_COUNT == 0)
                x86::lapic::StartCPUs(&cpus[startupIndex - x86::lapic::CPU_MAX_START_COUNT], &cpus[startupIndex]);
        }
    };

    /**
     * Actually do initializations.
     */
    for(auto const& header : madt)
    {
        switch(header.Type)
        {
            case EntryType::LAPIC:
            {
                auto entry = reinterpret_cast<const LAPICEntry&>(header);
                LOGV("LAPIC, ACPI UId: %hu, Id: %hhu, %s\n", entry.ACPIUId, entry.Id,
                    (entry.Flags & 0x1) ? "enabled" : "disabled");

                ProcessCPU(entry.Id, entry.Flags);

                break;
            }

            case EntryType::LAPICX2:
            {
                auto entry = reinterpret_cast<const LAPICX2Entry&>(header);
                LOGV("LAPIC X2, ACPI UId: %hu, Id: %u, %s\n", entry.ACPIUId, entry.Id,
                    (entry.Flags & 0x1) ? "enabled" : "disabled");

                ProcessCPU(entry.Id, entry.Flags);

                break;
            }

            case EntryType::IOAPIC:
            {
                auto entry = reinterpret_cast<const IOAPICEntry&>(header);
                LOGV("IOAPIC, Id: %hhu, Address: %p, GSI base: %u\n", entry.Id, (void*)entry.Address, entry.GSIBase);

                x86::ioapic::Register(new x86::ioapic::IOAPIC(entry.GSIBase, entry.Address));

                break;
            }

            case EntryType::INT_SRC_OVERRIDE:
            {
                auto entry = reinterpret_cast<const IntSrcOverrideEntry&>(header);
                LOGV("Interrupt Source Override, Bus: %hhu, Source: %hhu, GSI: %u\n", entry.Bus, entry.Source, entry.GSI);
                break;
            }

            #if 0
            case EntryType::LAPIC_ADDR_OVERRIDE:
            {
                auto entry = reinterpret_cast<const LAPICAddrOverrideEntry&>(header);
                LOGV("LAPIC Address Override, Address: %llu\n", entry.LAPICAddress);

                #if defined(__i386__)
                if(entry.LAPICAddress < 0xFFFFFFFF)
                    LAPICAddress_ = (uintptr_t)entry.LAPICAddress;
                else
                    LOGV("Override in 64-bit range\n");
                #else
                LAPICAddress_ = entry.LAPICAddress;
                #endif

                break;
            }
            #endif

            default:
                LOGV("entry %hhu\n", static_cast<uint8_t>(header.Type));
                break;
        }
    }

    /**
     * If the CPUs is not a multiple of CPU_MAX_START_COUNT, then it's possible
     * there are remaining CPUs that did not get started inside the loop.
     * Process them here.
     */
    if(startupIndex > 0)
    {
        cpuid_t start = startupIndex - (startupIndex % x86::lapic::CPU_MAX_START_COUNT);
        x86::lapic::StartCPUs(&cpus[start], &cpus[startupIndex]);
    }

    --cpus;
    cpu::SetCPUs(cpus, cpuCount); // Wss niet meer nodig?
}

}
