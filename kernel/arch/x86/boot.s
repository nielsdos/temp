/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

.set MB_MAGIC,     0xE85250D6   /* Multiboot magic */
.set MB_ARCH,      0            /* i386 protected mode */
.set TAG_REQUIRED, 0            /* Required tag */
.set TAG_OPTIONAL, 1            /* Optional tag */

/* Multiboot header */
.section .mboot
.align 8
MBootHeaderStart:
.long MB_MAGIC
.long MB_ARCH
.long MBootHeaderEnd - MBootHeaderStart
.long -(MB_MAGIC + MB_ARCH + (MBootHeaderEnd - MBootHeaderStart))

/* Information request tag */
.align 8
InfoReqStart:
.word 1                         /* Type */
.word TAG_REQUIRED              /* Flags */
.long InfoReqEnd - InfoReqStart /* Size of this tag */
.long 1                         /* Request: command line */
.long 6                         /* Request: memory map */
.long 15                        /* Request: ACPI */
InfoReqEnd:

/* Framebuffer tag */
.align 8
LFBTagStart:
.word 5                         /* Type */
.word TAG_REQUIRED              /* Flags */
.long LFBTagEnd - LFBTagStart   /* Size of this tag */
.long 1024                      /* Width (can be overriden in grub.cfg) */
.long 768                       /* Height (can be overriden in grub.cfg) */
.long 32                        /* Depth */
LFBTagEnd:

/* End tag */
.align 8
EndTagStart:
.word 0                         /* Type */
.word 0                         /* Flags */
.long EndTagEnd - EndTagStart   /* Size of this tag */
EndTagEnd:
MBootHeaderEnd:

/* Preallocate for paging */
.section .bss, "aw", @nobits
.align 0x1000
.global BootPDir
BootPDir:
.skip 0x1000
BootPTbl1:
.skip 0x1000
BootPTbl2:
.skip 0x1000

/* Code */
.section .text

.extern KernelMain
.type KernelMain, @function

.macro PAGE_MAP start count flags
    movl $(BootPTbl1 + (\start >> 12) * 4), %edi
    movl $(\start + \flags), %esi
    movl $\count, %ecx
1:
    movl %esi, (%edi)
    addl $0x1000, %esi
    addl $4, %edi
    loop 1b
.endm

.global Start
.type Start, @function
Start:
    cld

    # TODO
    movl $(BootPTbl1 + 0x3), BootPDir + 0 * 4
    movl $(BootPTbl2 + 0x3), BootPDir + 768 * 4
    movl $(BootPDir + 0x3), BootPDir + 1023 * 4
    PAGE_MAP 0x1000 1023 0x303
    PAGE_MAP 0x100000 _ldRODataPages 0x301

    /**
     * Setup PAT
     * Keep the lower half the same as on startup defaults, but modify the higher half
     * PATs in order (lower):  WB, WT, UC-, UC (same as defaults)
     *               (higher): WC, WP, *reserved*, *reserved*
     */
    movl $(0x06 << 0 | 0x04 << 8 | 0x07 << 16 | 0x00 << 24), %eax
    movl $(0x01 << 0 | 0x05 << 8 | 0x00 << 16 | 0x00 << 24), %edx
    movl $0x0277, %ecx
    wrmsr

    /* Prepare paging: Set CR3 and set PGE bit in CR4 */
    movl $BootPDir, %ecx
    movl %ecx, %cr3
    movl %cr4, %ecx
    or $0x80, %cx
    movl %ecx, %cr4

    /* Load GDT */
    lgdt GDTDescriptor

    movw $0x10, %cx
    movw %cx, %ds
    movw %cx, %es
    movw %cx, %fs
    movw %cx, %gs
    movw %cx, %ss

    ljmp $0x08, $1f
1:
    /**
     * Fix alignment: we need 16-byte align at call time
     * Fix ebx (tags): point to first tag
     */
    movl $(StackTop - 12), %esp
    addl $8, %ebx
    pushl %ebx
    call _init
    call KernelMain

    cli
1:
    hlt
    jmp 1b

.section .rodata
GDT:
.quad 0x0000000000000000    /* NULL */
.quad 0x00CF9A000000FFFF    /* Kernel code segment */
.quad 0x00CF92000000FFFF    /* Kernel data segment */
GDTDescriptor:
.word GDTDescriptor - GDT - 1
.long GDT

.section .stack, "aw", @nobits
StackBottom:
.skip 16384
StackTop:
