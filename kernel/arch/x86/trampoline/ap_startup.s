/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

.set AP_CPU_COUNT,     0x7C00   /* CPU count */
.set AP_PAGEDIR,       0x7C04   /* Physical address of location that holds pagedir pointer */
.set AP_START_POINTER, 0x7C08   /* Location of pointer to AP kernel entrypoint */
.set AP_CPU_DATA,      0x7C0C   /* Location of pointer to current AP data */

.section .trampoline
.code16

.global APEntry
.type APEntry, @function
APEntry:
    cli
    cld

    /* Load GDT */
    lgdt GDTDescriptor

    /* Protected mode */
    movl %cr0, %eax
    or $0x01, %eax
    movl %eax, %cr0

    ljmp $0x08, $1f

.code32
.align 4
1:
    movw $0x10, %cx
    movw %cx, %ds
    movw %cx, %es
    movw %cx, %fs
    movw %cx, %gs
    movw %cx, %ss

    /* Setup PAT, see boot.s */
    movl $(0x06 << 0 | 0x04 << 8 | 0x07 << 16 | 0x00 << 24), %eax
    movl $(0x01 << 0 | 0x05 << 8 | 0x00 << 16 | 0x00 << 24), %edx
    mov $0x0277, %ecx
    wrmsr

    /* Enable paging: PG and WP bit in CR0 and PGE bit in CR4 */
    movl AP_PAGEDIR, %ecx
    movl %ecx, %cr3
    movl %cr4, %ecx
    or $0x80, %cx
    movl %ecx, %cr4
    movl %cr0, %ecx
    orl $0x80010000, %ecx
    movl %ecx, %cr0

    /* Lock for getting our stack pointer */
2:
    testl $1, SPLock
    jnz 2b
    lock bts $0, SPLock
    jc 2b

    /* Get a new stack and per-CPU data pointer */
    movl AP_CPU_DATA, %eax
    movl (%eax), %esp
    movl 4(%eax), %eax
    addl $8, AP_CPU_DATA

    /* Increase counter, we're still in a loop */
    incl AP_CPU_COUNT

    movl $0, SPLock

    /* Set per-CPU data */
    xorl %edx, %edx
    movl $0xC0000100, %ecx
    wrmsr

    jmpl *AP_START_POINTER
.hang:
    cli
    hlt
    jmp .hang

.align 4
SPLock:
.long 0

.align 16
GDT:
.quad 0x0000000000000000    /* NULL */
.quad 0x00CF9A000000FFFF    /* Kernel code segment */
.quad 0x00CF92000000FFFF    /* Kernel data segment */
GDTDescriptor:
.word GDTDescriptor - GDT - 1
.long GDT
