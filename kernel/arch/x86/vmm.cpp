/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <string.h>
#include <vmm.hpp>
#include <pmm.hpp>
#include <panic.hpp>
#include <compiler.hpp>

namespace mem::vmm
{

constexpr uint32_t KERNEL_TBL_FLAGS = MapFlags::WRITABLE | 0x1;

void Init()
{
    Enable();
    // TODO: specialise for numa etc?
    return;
    // Create kernel page tables so they can be shared later
    for(int i = 768; i < 1024; ++i)
    {
        if(PAGE_DIR[i] == 0)
        {
            /**
             * Note: if this wouldn't work,
             *       then the kernel wouldn't be able to work anyway.
             *       So don't bother with checking the return value.
             */
            uintptr_t newTbl = pmm::AllocPageUnlocked();
            PAGE_DIR[i] = newTbl | KERNEL_TBL_FLAGS;
            uint32_t* tbl = PAGE_TBL_BASE + 1024 * i;
            memset(tbl, 0, 4096);
        }
    }
}

uintptr_t GetPhysicalAddress(uintptr_t vaddr)
{
    uint32_t dirIndex = vaddr >> 22;
    uint32_t tblIndex = (vaddr >> PAGE_SHIFT) % 1024;

    if(unlikely(PAGE_DIR[dirIndex] == 0))
        return 0;

    uint32_t* tbl = PAGE_TBL_BASE + 1024 * dirIndex;
    return (tbl[tblIndex] & ~PAGE_MASK) + (vaddr & PAGE_MASK);
}

uint32_t MapWithDonatedFrames(uintptr_t vaddr, uintptr_t frames[2], mapflags_t flags)
{
    assert((vaddr & PAGE_MASK) == 0);
    assert((frames[0] & PAGE_MASK) == 0);
    assert((frames[1] & PAGE_MASK) == 0);

    uint32_t dirIndex  = vaddr >> 22;
    uint32_t pageIndex = (vaddr >> PAGE_SHIFT) % 1024;

    uint32_t i = 0;

    uint32_t* tbl = PAGE_TBL_BASE + 1024 * dirIndex;
    if(PAGE_DIR[dirIndex] == 0)
    {
        PAGE_DIR[dirIndex] = frames[i++] | KERNEL_TBL_FLAGS;
        memset(tbl, 0, 4096);
    }

    if(tbl[pageIndex] == 0)
        tbl[pageIndex] = frames[i++] | flags | 0x01;

    return i;
}

bool Map(uintptr_t vaddr, uintptr_t paddr, uint32_t flags)
{
    assert((vaddr &  PAGE_MASK) == 0);
    assert((paddr &  PAGE_MASK) == 0);
    assert((flags & ~PAGE_MASK) == 0);

    uint32_t dirIndex = vaddr >> 22;
    uint32_t tblIndex = (vaddr >> PAGE_SHIFT) % 1024;

    uint32_t* tbl = PAGE_TBL_BASE + 1024 * dirIndex;
    if(PAGE_DIR[dirIndex] == 0)
    {
        uintptr_t newTbl = pmm::AllocPage();
        if(unlikely(newTbl == 0))
            return false;

        PAGE_DIR[dirIndex] = newTbl | KERNEL_TBL_FLAGS;
        memset(tbl, 0, 4096);
    }

    assert(tbl[tblIndex] == 0 && "Would overwrite mapping");
    tbl[tblIndex] = paddr | flags | 0x1;

    /**
     * Some CPUs (mostly Cyrix) remember that a page was "not present".
     * But we don't care about them because they're too old to work on anyway.
     */
    #if 0
    InvalidatePage(vaddr);
    #endif

    return true;
}

// TODO: free phys pagetbl?
void UnMap(uintptr_t vaddr)
{
    assert((vaddr & PAGE_MASK) == 0);

    uint32_t dirIndex = vaddr >> 22;
    uint32_t tblIndex = (vaddr >> PAGE_SHIFT) % 1024;

    if(unlikely(PAGE_DIR[dirIndex] == 0))
        return;

    uint32_t* tbl = PAGE_TBL_BASE + 1024 * dirIndex;
    if(tbl[tblIndex] != 0)
    {
        tbl[tblIndex] = 0;
        InvalidatePage(vaddr);
    }
}

bool MapRange(uintptr_t vaddr, uintptr_t paddr, size_t size, uint32_t flags)
{
    for(size_t x = 0; x < size; x += PAGE_SIZE)
    {
        if(unlikely(!Map(vaddr + x, paddr + x, flags)))
        {
            UnmapRange(vaddr, x);
            return false;
        }
    }

    return true;
}

void UnmapRange(uintptr_t vaddr, size_t size)
{
    for(size_t x = 0; x < size; x += PAGE_SIZE)
        UnMap(vaddr + x);
}

}
