/**
 * BSD 3-Clause License
 * 
 * Copyright (c) 2018, The cokos project and its contributors
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NDEBUG

#include <cstdint>
#include <panic.hpp>

typedef uintptr_t ValueHandle_t;

enum class Kind
{
    INTEGER = 0,
    FLOAT = 1,
    OTHER = 0xFFFF
};

class TypeDescriptor
{
public:

    /**
     * Gets the type name
     * @return The name
     */
    const char* GetName() const
    {
        return _typeName;
    }

    /**
     * Gets the type kind
     * @return The kind
     */
    Kind GetKind() const
    {
        return static_cast<Kind>(_typeKind);
    }

    /**
     * Checks this is a signed int
     * @return True if this is a signed int
     */
    bool IsSignedInt() const
    {
        return (GetKind() == Kind::INTEGER && (_typeInfo & 1));
    }

    /**
     * Checks this is a signed int
     * @return True if this is a signed int
     */
    bool IsUnsignedInt() const
    {
        return (GetKind() == Kind::INTEGER && !(_typeInfo & 1));
    }

private:
    uint16_t _typeKind;
    uint16_t _typeInfo;
    char     _typeName[];
};

const char* TypeCheckKinds[] =
{
    "load of",
    "store to",
    "reference binding to",
    "member access within",
    "member call on",
    "constructor call on",
    "downcast of",
    "downcast of",
    "upcast of",
    "cast to virtual base of",
    "non-null binding to"
};

struct SourceLocation
{
    const char* Filename;
    uint32_t Line;
    uint32_t Column;
};

struct TypeMismatchData
{
    SourceLocation Location;
    const TypeDescriptor& Type;
    unsigned char LogAlignment;
    unsigned char TypeCheckKind;
};

struct OutOfBoundsData
{
    SourceLocation Location;
    const TypeDescriptor& ArrayType;
    const TypeDescriptor& IndexType;
};

struct ShiftOutOfBoundsData
{
    SourceLocation Location;
    const TypeDescriptor& LHSType;
    const TypeDescriptor& RHSType;
};

struct OverflowData
{
    SourceLocation Location;
    const TypeDescriptor& Type;
};

struct FunctionTypeMismatchData
{
    SourceLocation Location;
    const TypeDescriptor& Type;
};

struct NonNullArgData
{
    SourceLocation Location;
    SourceLocation AttrLocation;
    int ArgIndex;
};

struct UnreachableData
{
    SourceLocation Location;
};

struct VLABoundData
{
    SourceLocation Location;
    const TypeDescriptor& Type;
};

struct FloatCastOverflowData
{
    SourceLocation Location;
    const TypeDescriptor& FromType;
    const TypeDescriptor& ToType;
};

struct InvalidValueData
{
    SourceLocation Location;
    const TypeDescriptor& Type;
};

struct PointerOverflowData
{
    SourceLocation Location;
};

// Helper that also puts location in, so we don't need to type it over and over again
#define UBSAN_ABORT_NOARGS(str) runtime::Panic("ubsan [%s:%d:%d] " str, data->Location.Filename, data->Location.Line, data->Location.Column)
#define UBSAN_ABORT(str, ...)   runtime::Panic("ubsan [%s:%d:%d] " str, data->Location.Filename, data->Location.Line, data->Location.Column, __VA_ARGS__)

extern "C"
{

void __ubsan_handle_type_mismatch_v1(TypeMismatchData* data, intptr_t pointer)
{
    if(pointer == 0)
    {
        UBSAN_ABORT("null pointer access of type %s", TypeCheckKinds[data->TypeCheckKind]);
    }
    else if(pointer & (data->LogAlignment - 1))
    {
        UBSAN_ABORT("misaligned address %p for type %s, which requires %u alignment", (void*)pointer, TypeCheckKinds[data->TypeCheckKind], 1 << data->LogAlignment);
    }
    else
    {
        UBSAN_ABORT("address %p with insufficient space for type %s", (void*)pointer, TypeCheckKinds[data->TypeCheckKind]);
    }
}

void __ubsan_handle_out_of_bounds(OutOfBoundsData* data, ValueHandle_t index)
{
    UBSAN_ABORT("index %lu out of bounds for type %s", index, data->ArrayType.GetName());
}

void __ubsan_handle_shift_out_of_bounds(ShiftOutOfBoundsData* data, void* /* lhs */, void* /* rhs */)
{
    UBSAN_ABORT("shift out of bounds, lhs type %s, rhs type %s", data->LHSType.GetName(), data->RHSType.GetName());
}

void __ubsan_handle_function_type_mismatch(FunctionTypeMismatchData* data, ValueHandle_t /* function */)
{
    UBSAN_ABORT("call to a function through incorrect function pointer type %s", data->Type.GetName());
}

void __ubsan_handle_nonnull_arg(NonNullArgData* data)
{
    UBSAN_ABORT("null pointer passed as argument %d, which is declared to never be null\nspecified here: [%s:%d:%d]",
        data->ArgIndex, data->AttrLocation.Filename, data->AttrLocation.Line, data->AttrLocation.Column);
}

// Helper for common overflow handling code
#define OVERFLOW_HANDLER(name, op) \
void __ubsan_handle_##name(OverflowData* data, ValueHandle_t lhs, ValueHandle_t rhs) \
{ \
    if(data->Type.IsSignedInt()) \
        UBSAN_ABORT("signed integer overflow: %ld %s %ld cannot be represented in type %s", (intptr_t)lhs, op, (intptr_t)rhs, data->Type.GetName()); \
    else /* if(data->Type.isUnsignedInt()) */ \
        UBSAN_ABORT("unsigned integer overflow: %lu %s %lu cannot be represented in type %s", lhs, op, rhs, data->Type.GetName()); \
}

OVERFLOW_HANDLER(add_overflow, "+");
OVERFLOW_HANDLER(sub_overflow, "-");
OVERFLOW_HANDLER(mul_overflow, "*");

void __ubsan_handle_divrem_overflow(OverflowData* data, void* /* lhs */, void* rhs)
{
    if((intptr_t)rhs == -1)
        UBSAN_ABORT("division by -1 cannot be represented in type %s", data->Type.GetName());
    else
        UBSAN_ABORT("division by zero in type %s", data->Type.GetName());
}

void __ubsan_handle_negate_overflow(OverflowData* data, ValueHandle_t /* old */)
{
    UBSAN_ABORT("negation cannot be represented in type %s", data->Type.GetName());
}

void __ubsan_handle_builtin_unreachable(UnreachableData* data)
{
    UBSAN_ABORT_NOARGS("execution reached a __builtin_unreachable call");
}

void __ubsan_handle_missing_return(UnreachableData* data)
{
    UBSAN_ABORT_NOARGS("execution reached end of value-returning statement without a return value");
}

void __ubsan_handle_vla_bound_not_positive(VLABoundData* data, ValueHandle_t /* bound */)
{
    UBSAN_ABORT("variable length array bound evaluates to non-positive value, type %s", data->Type.GetName());
}

void __ubsan_handle_float_cast_overflow(FloatCastOverflowData* data, ValueHandle_t /* from */)
{
    UBSAN_ABORT("cast from %s to %s is outside the range of the type", data->FromType.GetName(), data->ToType.GetName());
}

void __ubsan_handle_load_invalid_value(InvalidValueData* data, void* /* data */)
{
    UBSAN_ABORT("load of value which is invalid for type %s", data->Type.GetName());
}

void __ubsan_handle_pointer_overflow(PointerOverflowData* data, ValueHandle_t /* base */, ValueHandle_t /* result */)
{
    UBSAN_ABORT_NOARGS("pointer overflow");
}

}

#endif