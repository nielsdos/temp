# Cokos

**insert badges and screenshots here**

Cokos is a hobby operating system built mostly from scratch. Its name comes from Communication Kernel based OS. It currently only runs on 32-bit x86. Support for 64-bit x86 is planned for the future.

This repository contains the kernel, userspace and some tools that help to setup a development environment.

## Building and running

The build process consists of two steps.
A docker container is planned for the future.

### Step one

The first step is to run *./configure*.
This script creates some configurations files that store which architecture you want etc. You can provide a target architecture or don't provide it to use the architecture on which you are currently running.

```bash
# For 32-bit x86, run:
./configure --target=i686
# For more info, run:
./configure --help
```

### Step two

The second step is to actually build a OS-specific toolchain. The easiest way is to let a script handle this for you.
```bash
cd toolchain
./create_toolchain
```
This will install the packages required to build the toolchain, get and patch the sources of the compiler and other buildtools and build the toolchain. It will also setup a virtual hard disk image with GRUB2 on it.
This can take some time.

After this is done, you are ready to build and run the OS.
```bash
# You are in the toolchain folder
# Go back to the project root folder
cd ..
make qemu
# This will compile the kernel, copy it to the disk image and launch qemu.

# If you just wish to compile the kernel without running qemu,
# and copy the kernel to the disk image, run:
make
```

#### About the sysroot folder

The sysroot folder in the project root folder contains the filesystem structure of your hard disk image. If you change something here, it will automatically be copied to the disk image by invoking *make*.

#### What packages are required?

The toolchain executes [toolchain/install_packages](toolchain/install_packages) to install the required packages for you. The script works on Debian derivatives, Arch and Fedora.
It will install following packages:
* gcc, binutils and make
* packages required to build gcc and binutils: autoconf, automake, ...
* qemu (but you can always use another emulator if you wish)
* wget
* grub (to run grub-install on the hard disk image)
* development packages for fuse (to compile bindfs)

We build bindfs ourselves because not all supported distros offer this in their repositories.
bindfs is required so we can copy files as non-root to the disk image.

#### Running system requirements

* 128 MB RAM
* Pentium 4 or more modern

## License

The original work has been licensed under 3-Clause BSD license.
See [LICENSE](LICENSE) for more info.
